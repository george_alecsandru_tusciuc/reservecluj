package eu.accesa.reservecluj.repo;

import eu.accesa.reservecluj.model.Reservation;
import eu.accesa.reservecluj.model.ReservationStatus;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.service.reservationService.ReservationServiceImpl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Date;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReservationServiceTest {
    @InjectMocks
    ReservationServiceImpl a = new ReservationServiceImpl();
    @Mock
    ReservationRepository reservationRepository;

    @Before
    public void setUp() throws Exception {
        a.setRepository(reservationRepository);
    }

    @Test
    public void findById() throws Exception {
        when(reservationRepository.findById(1L)).thenReturn(new Reservation(1L, "email", "1", LocalDate.of(1,1,1), 1, LocalTime.now(), new Restaurant(), ReservationStatus.PENDING));
        Assert.assertEquals(new Reservation(1L, "email", "1", LocalDate.of(1,1,1), 1, LocalTime.now(), new Restaurant(), ReservationStatus.PENDING), a.findOne(1L));
    }

    @Test
    public void findByEmail() throws Exception {
        when(reservationRepository.findByEmail("asa")).thenReturn(Arrays.asList(new Reservation(1L, "email", "1", LocalDate.of(1,1,1), 1, LocalTime.now(), new Restaurant(), ReservationStatus.PENDING)));
        Assert.assertEquals(Arrays.asList(new Reservation(1L, "email", "1", LocalDate.of(1,1,1), 1, LocalTime.now(), new Restaurant(), ReservationStatus.PENDING)), a.findByEmail("asa"));
    }

    @Test
    public void findByDate() throws Exception {

    }

    @Test
    public void findByHour() throws Exception {

    }

    @Test
    public void findByNrPersons() throws Exception {

    }

    @Test
    public void findByReservationStatus() throws Exception {

    }

    @Test
    public void findAllWithRestaurant() throws Exception {

    }

    @Test
    public void findOneWithRestaurant() throws Exception {

    }

}