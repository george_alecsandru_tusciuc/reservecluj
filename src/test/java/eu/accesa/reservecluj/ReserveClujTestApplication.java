package eu.accesa.reservecluj;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;

@EnableConfigurationProperties
@EnableAutoConfiguration
@TestPropertySource(locations = {"classpath:application.properties"})
@WebAppConfiguration
@SpringBootApplication
public class ReserveClujTestApplication {
    
    private static final Logger LOG = LoggerFactory.getLogger(ReserveClujTestApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ReserveClujTestApplication.class, args);
    }
}