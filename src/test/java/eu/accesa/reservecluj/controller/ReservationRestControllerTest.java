package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.ReserveClujTestApplication;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReserveClujTestApplication.class)
@WebIntegrationTest("server.port=8080")
@Sql(scripts = "classpath:insert.sql")
@Sql(scripts = "classpath:delete.sql", executionPhase = AFTER_TEST_METHOD)
public class ReservationRestControllerTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown(){
    }
    @Test
    public void findAll() throws Exception {
        given().contentType("application/json")
                .when().get("reservation").then()
                .statusCode(200);
    }
    @Test
    public void findOne() throws Exception {
        given().contentType("application/json")
                .when().get("reservation/1").then()
                .body(containsString("\"id\":1"))
                .body(containsString("\"email\":\"reservationsCluj@gmail.com\""))
                .body(containsString("telephoneNumber"))
                .body(containsString("date"))
                .body(containsString("nrPersons"))
                .body(containsString("hour"))
                .body(containsString("restaurant"))
                .body(containsString("reservationStatus"))
                .statusCode(200);
    }
    @Test
    public void deleteReservation() throws Exception {
        given().contentType("application/json")
                .when().delete("reservation/1030").then()
                .statusCode(400);
    }
    @Test
    public void sendMail() throws Exception {

    }

    @Test
    public void changeStatus() throws Exception {
        given().contentType("application/json")
                .when().post("reservation/1/status?status=DONE").then()
                .statusCode(200);
        given().contentType("application/json")
                .when().get("reservation/1").then()
                .body(containsString("\"id\":1"))
                .body(containsString("\"reservationStatus\":\"DONE\""))
                .statusCode(200);
    }
}
