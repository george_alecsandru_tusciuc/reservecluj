package eu.accesa.reservecluj.controller.scheduleController;

import eu.accesa.reservecluj.ReserveClujTestApplication;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.isEmptyOrNullString;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReserveClujTestApplication.class)
@WebIntegrationTest("server.port=8080")
@Sql(scripts = "classpath:insert.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "classpath:delete.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class ScheduleRestControllerTest {

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addSchedule() throws Exception {
        given().contentType("application/json")
                .when().get("restaurant/1/schedule").then()
                .body(containsString("workDaysOpeningHour"))
                .body(containsString("\"id\":2"))
                .body(containsString("workDaysClosingHour"))
                .body(containsString("weekendDaysOpeningHour"))
                .body(containsString("weekendDaysClosingHour"))
                .statusCode(200);
    }

    @Test
    public void updateSchedule() throws Exception {
        given().contentType("application/json").
                body("{\n" +
                        "  \"workDaysOpeningHour\": [\n" +
                        "    1,\n" +
                        "    2\n" +
                        "  ],\n" +
                        "  \"workDaysClosingHour\": [\n" +
                        "    20,\n" +
                        "    2\n" +
                        "  ],\n" +
                        "  \"weekendDaysOpeningHour\": [\n" +
                        "    1,\n" +
                        "    2\n" +
                        "  ],\n" +
                        "  \"weekendDaysClosingHour\": [\n" +
                        "    20,\n" +
                        "    2\n" +
                        "  ]\n" +
                        "}")
                .when().put("restaurant/1/schedule").then()
                .statusCode(200).body(containsString("\"id\":2"));
    }

    @Test
    public void getSchedule() throws Exception {
        given().contentType("application/json")
                .when().get("restaurant/10/schedule").then()
                .body(isEmptyOrNullString())
                .statusCode(200);
        given().contentType("application/json")
                .when().get("restaurant/1/schedule").then()
                .body(containsString("\"id\":2"))
                .statusCode(200);
    }

    @Test
    public void deleteSchedule() throws Exception {
        given().contentType("application/json").when()
                .delete("restaurant/-1/schedule").then().statusCode(400);

    }

    @Test
    public void checkOpenStatus() throws Exception {

        given().contentType("application/json")
                .when().get("restaurant/1/schedule/status?timeStamp=1468942320").then()
                .body(containsString("Open"))
                .statusCode(200);


        given().contentType("application/json")
                .when().get("restaurant/10/schedule/status?timeStamp=1468942320").then()
                .body(containsString("no schedule"))
                .statusCode(400);
    }

    @Test
    public void handleDataAccessException() throws Exception {

    }

}