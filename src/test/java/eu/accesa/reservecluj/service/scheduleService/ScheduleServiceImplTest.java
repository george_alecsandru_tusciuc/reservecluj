package eu.accesa.reservecluj.service.scheduleService;


import eu.accesa.reservecluj.model.Schedule;
import eu.accesa.reservecluj.repo.ScheduleRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScheduleServiceImplTest {

    @InjectMocks
    private ScheduleService scheduleService= new ScheduleServiceImpl();
    @Mock
    private ScheduleRepository scheduleRepository=mock(ScheduleRepository.class);
    @Before
    public void setUp() {
        scheduleService.setScheduleRepository(scheduleRepository);
        LocalTime testWorkOpen = LocalTime.of(8, 30);
        LocalTime testWorkClose = LocalTime.of(20, 30);
        LocalTime testWorkOpenWeekend=LocalTime.of(10,0);
        LocalTime testWorkCloseWeekend=LocalTime.of(18,20);
        when(scheduleRepository.findByRestaurantId(anyLong())).thenReturn(new Schedule(testWorkOpen, testWorkClose,
                                                                                    testWorkOpenWeekend, testWorkCloseWeekend));
    }
    @Test
    public void checkOpenStatusWithDifferentTimes() throws Exception {
        Instant testClientTime=Instant.ofEpochSecond(1468936980);
        assertEquals("Open",scheduleService.checkOpenStatus(testClientTime,anyLong()));


        OffsetDateTime testTime= OffsetDateTime.of(LocalDate.of(2016,9,17), LocalTime.of(21,30), ZoneOffset.UTC);
        testClientTime=testTime.toInstant();
        assertEquals("Closed",scheduleService.checkOpenStatus(testClientTime,anyLong()));


        testTime= OffsetDateTime.of(LocalDate.of(2016,7,21), LocalTime.of(20,30), ZoneOffset.UTC);
        testClientTime=testTime.toInstant();
        assertEquals("Closed",scheduleService.checkOpenStatus(testClientTime,anyLong()));

        testTime= OffsetDateTime.of(LocalDate.of(2016,7,21), LocalTime.of(18,29), ZoneOffset.UTC);
        testClientTime=testTime.toInstant();
        assertEquals("Open",scheduleService.checkOpenStatus(testClientTime,anyLong()));

        testTime= OffsetDateTime.of(LocalDate.of(2016,7,21), LocalTime.of(18,30), ZoneOffset.UTC);
        testClientTime=testTime.toInstant();
        assertEquals("Closed",scheduleService.checkOpenStatus(testClientTime,anyLong()));

        testTime= OffsetDateTime.of(LocalDate.of(2016,7,23), LocalTime.of(6,00), ZoneOffset.UTC);
        testClientTime=testTime.toInstant();
        assertEquals("Closed",scheduleService.checkOpenStatus(testClientTime,anyLong()));

        testTime= OffsetDateTime.of(LocalDate.of(2016,7,23), LocalTime.of(18,00), ZoneOffset.UTC);
        testClientTime=testTime.toInstant();
        assertEquals("Closed",scheduleService.checkOpenStatus(testClientTime,anyLong()));

        testTime= OffsetDateTime.of(LocalDate.of(2016,7,23), LocalTime.of(14,00), ZoneOffset.UTC);
        testClientTime=testTime.toInstant();
        assertEquals("Open",scheduleService.checkOpenStatus(testClientTime,anyLong()));

    }

}