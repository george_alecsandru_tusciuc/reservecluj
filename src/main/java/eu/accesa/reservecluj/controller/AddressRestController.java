package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.model.Address;
import eu.accesa.reservecluj.service.addressService.AddressService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("restaurant/{id}/address")
public class AddressRestController {

    private static final Log LOG = LogFactory.getLog(AddressRestController.class);

    @Autowired
    private AddressService addressServiceImpl;

    @RequestMapping(method = RequestMethod.GET)
    public Address findAddressForRestaurant(@PathVariable long id) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Find address for restaurant with id = " + id + "operation initiated.");
        }
        return addressServiceImpl.findAddressForRestaurant(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public Address changeAddressForRestaurant(@PathVariable long id, @RequestBody @Valid Address address) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Change address for restaurant with id = " + id + " operation initiated for " + address);
        }
        return addressServiceImpl.changeAddressForRestaurant(id, address);
    }
}
