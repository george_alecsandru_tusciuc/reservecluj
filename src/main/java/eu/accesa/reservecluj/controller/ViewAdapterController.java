package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.model.Feature;
import eu.accesa.reservecluj.service.featureService.FeatureCategoryService;
import eu.accesa.reservecluj.service.featureService.RestaurantFeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ViewAdapterController {
    @Autowired
    private FeatureCategoryService featureCategoryService;

    @Autowired
    private RestaurantFeatureService restaurantFeatureService;

    @RequestMapping(value = "/filters", method = RequestMethod.GET)
    public Map<String, List<String>> getFeaturesByCategory() {
        Map<String, List<String>> featureMap = new HashMap<>();
        List<Feature> features = restaurantFeatureService.getAllFeatures();
        for (Feature f : features) {
            featureMap.putIfAbsent(f.getCategory().getName(), new ArrayList<>());
            featureMap.get(f.getCategory().getName()).add(f.getName());
        }
        return featureMap;
    }
}
