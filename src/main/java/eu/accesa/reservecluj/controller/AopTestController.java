package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.service.security.ReserveSecured;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class AopTestController {

    @RequestMapping(value = "/aop/{id}", method = RequestMethod.GET)
    @ReserveSecured(isIdFirstParam = true)
    public Map<String, Long> assocMember(@PathVariable("id") Long testValue, @RequestHeader("Authorization") String token) {
        Map<String, Long> response = new HashMap<>();
        response.put("test", testValue);
        return response;
    }
}
