package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.model.Picture;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.service.pictureService.PictureService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("restaurant")
public class PictureRestController {

    private static final Logger LOG = LoggerFactory.getLogger(PictureRestController.class);
    @Autowired
    PictureService pictureService;

    @RequestMapping(method = RequestMethod.POST, value = "/{restaurantId}/pictures")
    @ResponseBody
    public Picture handleFileUpload(@PathVariable Long restaurantId,
                                    @RequestParam("path") MultipartFile file,
                                    RedirectAttributes redirectAttributes) {
        return pictureService.uploadImage(restaurantId, file, redirectAttributes);
    }

    /**
     * Adds an image to a restaurant in the database
     * @param   restaurantId The id of the restaurant
     * @param   path An absolute path of the image's URL
     * @return  the image at the specified URL
     */
    @RequestMapping(method = RequestMethod.POST, value = "/{restaurantId}/pictures/url")
    public Picture addImage(@PathVariable Long restaurantId,
                            @RequestBody String path){
        Picture picture;
        try {
            picture = new Picture();
            path = java.net.URLDecoder.decode(path,"UTF-8");
            path = path.substring(path.indexOf("=")+1);
            picture.setPath(path);
            Restaurant restaurant = new Restaurant();
            restaurant.setId(restaurantId);
            picture.setRestaurant(restaurant);
        } catch (UnsupportedEncodingException e) {
            LOG.warn("Illegal picture path with restaurant id: " + restaurantId);
            return null;
        }
        return pictureService.addImage(picture);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{restaurantId}/pictures")
    @ResponseBody
    public List<Picture> getPictures(@PathVariable Long restaurantId) {
        return pictureService.findByRestaurant(restaurantId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{restaurantId}/pictures/{pictureId}")
    public void deletePicture(@PathVariable Long restaurantId,
                              @PathVariable Long pictureId) {
        pictureService.delete(restaurantId, pictureId);
    }

    /**
     * Updates a restaurant picture in the database
     * @param restaurantId   The id of the restaurant
     * @param pictureId      The id of the picture that is going to be updated
     * @param path           An absolute URL of the new picture
     * @return               The image at the specified URL
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/{restaurantId}/pictures/{pictureId}")
    public Picture updatePicture(@PathVariable Long restaurantId,
                                 @PathVariable Long pictureId,
                                 @RequestBody String path){
        Picture picture;
        try {
            picture = new Picture();
            picture.setId(pictureId);
            path = java.net.URLDecoder.decode(path,"UTF-8");
            path = path.substring(path.indexOf("=")+1);
            picture.setPath(path);
            Restaurant r = new Restaurant();
            r.setId(restaurantId);
            picture.setRestaurant(r);
        } catch (UnsupportedEncodingException e) {
            LOG.warn("Illegal picture path with restaurant id: " + restaurantId);
            return null;
        }
        
        return pictureService.save(picture);
    }
}
