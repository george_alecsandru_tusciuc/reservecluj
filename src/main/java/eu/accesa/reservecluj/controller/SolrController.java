package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.controller.dto.RestaurantDto;
import eu.accesa.reservecluj.service.solrService.SolrService;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class SolrController {
    @Autowired
    private SolrService solrService;

    @RequestMapping(value = "/solr", method = RequestMethod.DELETE)
    public ResponseEntity deleteSolrIndexes() {
        try {
            solrService.deleteAllIndexes();
            return new ResponseEntity(HttpStatus.OK);
        } catch (IOException | SolrServerException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/solr", method = RequestMethod.POST)
    public ResponseEntity addSolrIndexes() {
        try {
            solrService.addAllIndexes();
            return new ResponseEntity(HttpStatus.OK);
        } catch (IOException | SolrServerException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
