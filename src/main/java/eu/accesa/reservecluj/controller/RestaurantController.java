package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.service.restaurantService.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("thymeleaf/restaurant")
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    @RequestMapping(method = RequestMethod.GET)
    public String getAll(Model model) {
        model.addAttribute("restaurants", restaurantService.getAllRestaurants());
        return "index";
    }

    @RequestMapping(value = "/email", method = RequestMethod.GET)
    public String show(Model model) {
        model.addAttribute("status", "DENIED");
        model.addAttribute("user", "A");
        return "eMail";
    }
}
