package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.controller.dto.ReviewDto;
import eu.accesa.reservecluj.model.Reservation;
import eu.accesa.reservecluj.model.Review;
import eu.accesa.reservecluj.service.reservationService.ReservationService;
import eu.accesa.reservecluj.service.restaurantService.RestaurantService;
import eu.accesa.reservecluj.service.reviewService.ReviewService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.persistence.EntityExistsException;

@RestController
public class ReviewRestController {
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private ReservationService reservationService;

    @RequestMapping(value = "/restaurant/{restaurantId}/review", method = RequestMethod.GET)
    private ResponseEntity<List<Review>> findAllForRestaurant(@PathVariable Long restaurantId){
        return new ResponseEntity<>(reviewService.findByRestaurantId(restaurantId), HttpStatus.OK);
    }

    /**
     * Adds a review to a restaurant in the database, based on a reservation
     * @param restaurantId    The id of the restaurant
     * @param reservationId   The id of the reservation
     * @param review          A review
     * @return                Returns the review added, and HTTPStatus.OK
     */
    @RequestMapping(value = "/restaurant/{restaurantId}/reservation/{reservationId}/review",method = RequestMethod.POST)
    private ResponseEntity<?> addReview(@PathVariable("restaurantId") Long restaurantId,
                                        @PathVariable("reservationId") Long reservationId,
                                        @RequestBody Review review){
        Reservation reservation=reservationService.findByIdandRestaurantId(reservationId,restaurantId);
        if(reservation==null){
            throw new EntityExistsException("Reservation does not exist for the speciefied restaurant");
        }
        review.setRestaurant(reservation.getRestaurant());
        review.setReservation(reservation);
        review=reviewService.addReview(review);
        return new ResponseEntity<>(review,HttpStatus.OK);
    }
    @RequestMapping(value = "/restaurant/{restaurantId}/reservation/{reservationId}/review",method = RequestMethod.GET)
    private ResponseEntity<Review> getReview(@PathVariable Long restaurantId,@PathVariable Long reservationId){
        return new ResponseEntity<>(reviewService.findByRestaurantAndReservation(restaurantId,reservationId),HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurant/{restaurantId}/review/{reviewId}",method = RequestMethod.DELETE)
    private ResponseEntity<?> deleteReview(@PathVariable Long restaurantId, @PathVariable Long reviewId){
        reviewService.deleteOne(reviewId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurant/{restaurantId}/review/}",method = RequestMethod.DELETE)
    private ResponseEntity<?> deleteAll(@PathVariable Long restaurantId){
        reviewService.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurant/{restaurantId}/review/all", method = RequestMethod.GET)
    private ResponseEntity<?> findRatingForRestaurant(@PathVariable Long restaurantId) {
        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setAmbienceRating(reviewService.getOverAllRating(restaurantId, Review::getAmbienceRating));
        reviewDto.setServiceRating(reviewService.getOverAllRating(restaurantId, Review::getServiceRating));
        reviewDto.setFoodRating(reviewService.getOverAllRating(restaurantId, Review::getFoodRating));
        reviewDto.setNoiseRating(reviewService.getOverallNoiseRatingForRestaurant(restaurantId));
        reviewDto.setValueRating(reviewService.getOverAllRating(restaurantId, Review::getValueRating));
        reviewDto.setRating(reviewService.getOverAllRating(restaurantId, reviewService::getReviewOverallRatingForReview));
        reviewDto.setRestaurantName(restaurantService.getRestaurantById(restaurantId).getName());
        return new ResponseEntity<>(reviewDto, HttpStatus.OK);
    }
}
