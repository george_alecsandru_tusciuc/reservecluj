package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.model.Client;
import eu.accesa.reservecluj.service.clientService.ClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientRestController {
    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "client/{clientId}", method = RequestMethod.GET)
    public Client readClient(@PathVariable Long clientId) {
        return clientService.getOne(clientId);
    }

    @RequestMapping(value = "client", method = RequestMethod.POST)
    public Client addNewClient(@RequestBody Client member) {
        Client member1 = clientService.addClient(member);
        return member1;
    }

    @RequestMapping(value = "client/{clientId}", method = RequestMethod.PUT)
    public Client updateClient(@PathVariable Long clientId, @RequestBody Client client) {
        return clientService.update(clientId, client);
    }
}
