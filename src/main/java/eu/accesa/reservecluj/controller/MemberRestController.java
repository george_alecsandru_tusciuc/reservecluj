package eu.accesa.reservecluj.controller;


import eu.accesa.reservecluj.model.Member;
import eu.accesa.reservecluj.service.memberService.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
//@RequestMapping("restaurant/{id}/member")
public class MemberRestController {

    @Autowired
    private MemberService memberService;

    @RequestMapping(value = "member", method = RequestMethod.GET)
    public List<Member> getAll() {
        return memberService.getAllMembers();
    }

    @RequestMapping(value = "member/{memberId}", method = RequestMethod.GET)
    Member readMember(@PathVariable Long memberId) {
        return this.memberService.getOne(memberId);
    }

    @RequestMapping(value = "member/token", method = RequestMethod.POST)
    Map<String, String> loginMember(@RequestBody Member member) {
        return memberService.loginMember(member);
    }

    @RequestMapping(value = "member", method = RequestMethod.POST)
    public Member addNewMember(@RequestBody Member member) {
        Member member1 = memberService.addMember(member);
        return member1;
    }

    @RequestMapping(value = "member/{memberId}", method = RequestMethod.PUT)
    String updateMember(@PathVariable Long memberId, @RequestBody Member member) {
        return this.memberService.update(memberId, member);
    }

    @RequestMapping(value = "restaurant/{restaurantId}/member/{memberId}", method = RequestMethod.PUT)
    String updateMemberRestaurant(@PathVariable Long memberId, @PathVariable Long restaurantId, @RequestBody Member member) {
        return this.memberService.updateWithRestaurant(memberId, restaurantId, member);
    }
}
