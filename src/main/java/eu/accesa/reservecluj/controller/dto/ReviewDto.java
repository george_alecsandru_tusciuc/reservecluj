package eu.accesa.reservecluj.controller.dto;

import eu.accesa.reservecluj.model.NoiseRating;

public class ReviewDto {
    private Double rating;
    private Double foodRating;
    private Double ambienceRating;
    private Double serviceRating;
    private Double valueRating;
    private NoiseRating noiseRating;
    private String restaurantName;

    public String getRestaurantName() {
        return restaurantName;
    }

    public Double getServiceRating() {
        return serviceRating;
    }

    public void setServiceRating(Double serviceRating) {
        this.serviceRating = serviceRating;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Double getFoodRating() {
        return foodRating;
    }

    public void setFoodRating(Double foodRating) {
        this.foodRating = foodRating;
    }

    public Double getAmbienceRating() {
        return ambienceRating;
    }

    public void setAmbienceRating(Double ambienceRating) {
        this.ambienceRating = ambienceRating;
    }

    public Double getValueRating() {
        return valueRating;
    }

    public void setValueRating(Double valueRating) {
        this.valueRating = valueRating;
    }

    public NoiseRating getNoiseRating() {
        return noiseRating;
    }

    public void setNoiseRating(NoiseRating noiseRating) {
        this.noiseRating = noiseRating;
    }
}
