package eu.accesa.reservecluj.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class RestaurantFeatureSearchDTO {

    private List<String> general = new ArrayList<>();

    private List<String> quisine = new ArrayList<>();

    public List<String> getGeneral() {
        return general;
    }

    public void setGeneral(List<String> general) {
        this.general = general;
    }

    public List<String> getQuisine() {
        return quisine;
    }

    public void setQuisine(List<String> quisine) {
        this.quisine = quisine;
    }
}
