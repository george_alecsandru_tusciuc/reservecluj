package eu.accesa.reservecluj.controller.dto;

import eu.accesa.reservecluj.model.Address;
import eu.accesa.reservecluj.model.Feature;
import eu.accesa.reservecluj.model.Schedule;

import java.util.List;

public class RestaurantDto {
    private Long id;
    private List<Feature> features;
    private Schedule schedule;
    private Address address;
    private String name;
    private String description;
    private String linkSocial;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLinkSocial() {
        return linkSocial;
    }

    public void setLinkSocial(String linkSocial) {
        this.linkSocial = linkSocial;
    }
}
