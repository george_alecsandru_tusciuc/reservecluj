package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.model.Reservation;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.service.reservationService.ReservationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.validation.Valid;


@RestController
public class ReservationRestController {
    @Autowired
    private ReservationService reservationService;

    @RequestMapping(value = "/reservation", method = {RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity addReservation(@RequestParam Long restaurantId, @RequestBody @Valid Reservation reservation) {
        reservationService.addReservation(restaurantId,reservation);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/reservation", method = RequestMethod.GET)
    public ResponseEntity findAll() {
        return new ResponseEntity<>(reservationService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/reservation/{reservationId}", method = RequestMethod.GET)
    public ResponseEntity findOne(@PathVariable Long reservationId) {
        return new ResponseEntity<>(reservationService.findOne(reservationId), HttpStatus.OK);
    }

    @RequestMapping(value = "/reservation/{reservationId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteReservation(@PathVariable Long reservationId) {
        reservationService.delete(reservationId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "reservation/{reservationId}/confirmation", method = RequestMethod.POST)
    public ResponseEntity sendConfirmationMail(@PathVariable Long reservationId,@RequestBody String message) throws ExecutionException, InterruptedException {
        Reservation e = reservationService.findOne(reservationId);
        String result = reservationService.sendConfirmationMail(message, e.getEmail(),"Reservation Status", e, "eMail");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "reservation/{reservationId}/review", method = RequestMethod.POST)
    public ResponseEntity sendReviewEmail(@PathVariable Long reservationId) throws ExecutionException, InterruptedException {
        Reservation e = reservationService.findOne(reservationId);
        String result = reservationService.sendReviewMail(e.getEmail(), "Reservation Review", e, "review");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "reservation/{reservationId}/status", method = RequestMethod.PUT)
    public ResponseEntity changeStatus(@PathVariable Long reservationId, @RequestBody String status) {
        reservationService.updateStatus(reservationService.findOne(reservationId), status);
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value = "restaurant/{restaurantId}/reservations", method = RequestMethod.GET)
    public List<Reservation> getReservationsByRestaurant(@PathVariable Long restaurantId) {
        Restaurant restaurant = new Restaurant();
        restaurant.setId(restaurantId);
        return reservationService.findByRestaurant(restaurant);
    }

    @RequestMapping(value = "reservation/mail", method = RequestMethod.GET)
    public ResponseEntity<?> getByEmail(@RequestParam String reservationEmail){
        return new ResponseEntity<>( reservationService.findByEmailOrderedByDateAscFirst10(reservationEmail), HttpStatus.OK);
    }

}
