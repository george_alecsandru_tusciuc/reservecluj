package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.controller.dto.RestaurantDto;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.service.restaurantService.RestaurantFacade;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("restaurant")
public class RestaurantRestController {

    @Autowired
    private RestaurantFacade restaurantFacade;

    @RequestMapping(value = "/search")
    public ResponseEntity<List<RestaurantDto>> solrSearch(@RequestParam String query) {
        try {
            return new ResponseEntity<>(restaurantFacade.solrSearch(query), HttpStatus.OK);
        } catch (SolrServerException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Restaurant> getAll() {
        return restaurantFacade.getAllRestaurants();
    }

    @RequestMapping(value = "/{restaurantId}", method = RequestMethod.GET)
    public ResponseEntity<?> readRestaurant(@PathVariable Long restaurantId) {
        return new ResponseEntity<>(restaurantFacade.getRestaurantById(restaurantId), HttpStatus.OK);
    }

    @RequestMapping(value = "/{restaurantId}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateRestaurant(@PathVariable Long restaurantId, @RequestBody RestaurantDto res) throws IOException, SolrServerException {
        restaurantFacade.updateRestaurant(restaurantId, res);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<?> addNewRestaurant(@RequestBody Restaurant restaurant) {
        return new ResponseEntity<>(restaurantFacade.insertRestaurant(restaurant), HttpStatus.OK);
    }

    @RequestMapping(value = "/favorites", method = RequestMethod.GET)
    public List<Restaurant> getFavorites(@RequestParam String userEmail){
        return restaurantFacade.getFavoritesForUser(userEmail);
    }

    @RequestMapping(value = "/popular", method = RequestMethod.GET)
    public ResponseEntity<?> getMostPopular() {
        return new ResponseEntity<>(restaurantFacade.sortByRating(), HttpStatus.OK);
    }

    @RequestMapping(value = "/visited", method = RequestMethod.GET)
    public ResponseEntity<?> getMostVisited() {
        return new ResponseEntity<>(restaurantFacade.sortByNrOfVisits(), HttpStatus.OK);
    }

}
