package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.model.Schedule;
import eu.accesa.reservecluj.service.restaurantService.RestaurantService;
import eu.accesa.reservecluj.service.scheduleService.ScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@RestController
public class ScheduleRestController {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduleRestController.class);
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private RestaurantService restaurantService;

    @RequestMapping(value = "restaurant/{restaurantId}/schedule", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Schedule> addSchedule(@RequestBody @Valid Schedule schedule, @PathVariable Long restaurantId) {
        Restaurant restaurant = restaurantService.getRestaurantById(restaurantId);
        schedule.setRestaurant(restaurant);
        schedule = scheduleService.addSchedule(schedule);
        return new ResponseEntity<>(schedule, HttpStatus.OK);
    }

    @RequestMapping(value = "restaurant/{restaurantId}/schedule", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<Schedule> updateSchedule(@RequestBody @Valid Schedule schedule, @PathVariable Long restaurantId) {
        return new ResponseEntity<>(scheduleService.updateSchedule(schedule, restaurantId), HttpStatus.OK);
    }

    @RequestMapping(value = "restaurant/{restaurantId}/schedule", method = RequestMethod.GET)
    public ResponseEntity<Schedule> getSchedule(@PathVariable Long restaurantId) {
        return new ResponseEntity<>(scheduleService.findByRestaurant(restaurantId), HttpStatus.OK);
    }

    @RequestMapping(value = "restaurant/{restaurantId}/schedule", method = RequestMethod.DELETE)
    public ResponseEntity deleteSchedule(@PathVariable Long restaurantId) {
        Schedule schedule = scheduleService.findByRestaurant(restaurantId);
        scheduleService.deleteSchedule(schedule.getId());
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurant/{restaurantId}/schedule/status", method = RequestMethod.GET)
    public ResponseEntity<String> checkOpenStatus(@RequestParam @NotNull Long timeStamp, @PathVariable Long restaurantId) {
        Instant time = Instant.ofEpochSecond(timeStamp);
        return new ResponseEntity<>(scheduleService.checkOpenStatus(time, restaurantId), HttpStatus.OK);
    }

    @ExceptionHandler(DataAccessException.class)
    public ResponseEntity<String> handleDataAccessException(Exception ex) {
        LOG.warn("DataAccessException " + ex.getMessage());
        return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
