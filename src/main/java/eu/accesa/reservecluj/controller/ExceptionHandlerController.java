package eu.accesa.reservecluj.controller;

import com.auth0.jwt.JWTVerifyException;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(SolrServerException.class)
    public ResponseEntity<?> processSolrExceptions(SolrServerException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity processValidationError(IllegalArgumentException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.toString());
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity processRuntimeException(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.toString());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, String> processValidationError(MethodArgumentNotValidException exception) {
        BindingResult bindingResult = exception.getBindingResult();
        Map<String, String> fieldErrors = new HashMap<>();
        for (FieldError error : bindingResult.getFieldErrors()) {
            String currentFieldError = error.getField();
            if (fieldErrors.containsKey(currentFieldError)) {
                StringBuilder messagesForCurrentField = new StringBuilder();
                messagesForCurrentField.append(fieldErrors.get(currentFieldError));
                messagesForCurrentField.append(System.getProperty("line.separator"));
                messagesForCurrentField.append(error.getDefaultMessage());
                fieldErrors.replace(currentFieldError, fieldErrors.get(currentFieldError), messagesForCurrentField.toString());
            } else {
                fieldErrors.put(error.getField(), error.getDefaultMessage());
            }
        }
        return fieldErrors;
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity processValidationError(DataIntegrityViolationException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.toString());
    }

    @ExceptionHandler({EntityExistsException.class})
    public ResponseEntity memberUsernameError(EntityExistsException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.toString());
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity memberNotFoundError(EntityNotFoundException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.toString());
    }

    @ExceptionHandler({JWTVerifyException.class})
    public ResponseEntity invalidToken(JWTVerifyException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.toString());
    }

    @ExceptionHandler({SecurityException.class})
    public ResponseEntity noAccess(SecurityException e) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.toString());
    }
}

