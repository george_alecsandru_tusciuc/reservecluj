package eu.accesa.reservecluj.controller;

import eu.accesa.reservecluj.controller.dto.FeatureDTO;
import eu.accesa.reservecluj.controller.dto.RestaurantFeatureSearchDTO;
import eu.accesa.reservecluj.converter.FeatureConverter;
import eu.accesa.reservecluj.model.Feature;
import eu.accesa.reservecluj.model.FeatureCategory;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.service.featureService.FeatureCategoryService;
import eu.accesa.reservecluj.service.featureService.RestaurantFeatureService;
import eu.accesa.reservecluj.service.restaurantService.RestaurantService;
import org.hibernate.metamodel.relational.IllegalIdentifierException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/")
public class RestaurantFeatureRestController {

    @Autowired
    RestaurantFeatureService restaurantFeatureService;

    @Autowired
    FeatureConverter featureConverter;

    @Autowired
    FeatureCategoryService featureCategoryService;

    @Autowired
    RestaurantService restaurantService;

    private static final Logger LOG = LoggerFactory.getLogger(RestaurantFeatureRestController.class);

    @RequestMapping(value = "feature", method = RequestMethod.GET)
    public List<Feature> getFeatures() {
        return restaurantFeatureService.getAllFeatures();
    }

    @RequestMapping(value = "features/category", method = RequestMethod.POST, consumes = "application/json")
    public FeatureCategory addFeatureCategory(@RequestBody String featureCategoryName) {
        FeatureCategory featureCategory = new FeatureCategory();
        featureCategory.setName(featureCategoryName);
        return featureCategoryService.addFeatureCategory(featureCategory);
    }

    @RequestMapping(value = "features/category", method = RequestMethod.GET)
    public List<FeatureCategory> getFeatureCategories() {
        return featureCategoryService.getCategories();
    }

    @RequestMapping(value = "feature/category/{categoryId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteFeatureCategory(@PathVariable long categoryId) {
        try {
            featureCategoryService.deleteFeatureCategory(categoryId);
        } catch (IllegalIdentifierException illegalId) {
            LOG.warn("Illegal identifier");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "restaurant/search", method = RequestMethod.POST)
    public List<Restaurant> search(@RequestBody RestaurantFeatureSearchDTO restaurantFeatureSearchDTO) {
        List<Restaurant> tmp = restaurantFeatureService.filterRestaurants(restaurantFeatureSearchDTO);
        return restaurantFeatureService.filterRestaurants(restaurantFeatureSearchDTO);
    }

    @RequestMapping(value = "restaurant/{restaurantId}/features", method = RequestMethod.GET)
    public List<Feature> getFeaturesByRestaurantId(@PathVariable long restaurantId) {
        try {
            return restaurantFeatureService.getFeaturesByRestaurantId(restaurantId);
        } catch (IllegalIdentifierException illegalId) {
            LOG.warn("Illegal identifier");
            return new ArrayList<>();
        }
    }

    @RequestMapping(value = "restaurant/{restaurantId}/features", method = RequestMethod.POST, consumes = "application/json")
    public Feature addRestaurantProperty(@PathVariable long restaurantId, @RequestBody FeatureDTO featureDTO) {
        Feature f = featureConverter.convertToRestaurantProperty(featureDTO);
        f.setRestaurants(Arrays.asList(restaurantService.getRestaurantById(restaurantId)));
        f = restaurantFeatureService.addRestaurantFeature(f);
        return f;
    }

    @RequestMapping(value = "restaurant/{restaurantId}/filters/{featureId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteRestaurantFeature(@PathVariable long restaurantId, @PathVariable long featureId) {
        try {
            restaurantFeatureService.deleteRestaurantFeature(featureId, restaurantId);
        } catch (IllegalIdentifierException illegalId) {
            LOG.warn("Illegal identifier");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}
