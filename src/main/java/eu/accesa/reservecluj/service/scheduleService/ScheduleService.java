package eu.accesa.reservecluj.service.scheduleService;


import eu.accesa.reservecluj.model.Schedule;
import eu.accesa.reservecluj.repo.ScheduleRepository;

import java.time.Instant;
import java.util.List;

public interface ScheduleService {
    Schedule addSchedule(Schedule schedule);

    void deleteSchedule(Long scheduleId);

    Schedule updateSchedule(Schedule schedule, Long restaurantId);

    List<Schedule> findAll();

    Schedule findOne(Long scheduleId);

    String checkOpenStatus(Instant time, Long restaurantid);

    ScheduleRepository getScheduleRepository();

    void setScheduleRepository(ScheduleRepository scheduleRepository);

    Schedule findByRestaurant(Long restaurantId);
}
