package eu.accesa.reservecluj.service.scheduleService;

import eu.accesa.reservecluj.model.Schedule;
import eu.accesa.reservecluj.repo.ScheduleRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import javax.persistence.EntityNotFoundException;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    private static final Logger LOG = LoggerFactory.getLogger(ScheduleServiceImpl.class);

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Override
    public Schedule addSchedule(Schedule schedule) {
        return scheduleRepository.save(schedule);
    }

    @Override
    public Schedule findByRestaurant(Long restaurantId) {
        return scheduleRepository.findByRestaurantId(restaurantId);
    }

    @Override
    public void deleteSchedule(Long scheduleId) {
        scheduleRepository.delete(scheduleId);
    }

    @Override
    public Schedule updateSchedule(Schedule schedule, Long restaurantId) {
        Schedule updatedSchedule = scheduleRepository.findByRestaurantId(restaurantId);
        updatedSchedule.setWeekendDaysClosingHour(schedule.getWeekendDaysClosingHour());
        updatedSchedule.setWeekendDaysOpeningHour(schedule.getWeekendDaysOpeningHour());
        updatedSchedule.setWorkDaysOpeningHour(schedule.getWorkDaysOpeningHour());
        updatedSchedule.setWorkDaysClosingHour(schedule.getWorkDaysClosingHour());
        return scheduleRepository.save(updatedSchedule);
    }

    @Override
    public List<Schedule> findAll() {
        return scheduleRepository.findAll();
    }

    @Override
    public Schedule findOne(Long scheduleId) {
        return scheduleRepository.findOne(scheduleId);
    }

    public ScheduleRepository getScheduleRepository() {
        return scheduleRepository;
    }

    public void setScheduleRepository(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    @Override
    public String checkOpenStatus(Instant time, Long restaurantId) {

        Schedule schedule = scheduleRepository.findByRestaurantId(restaurantId);

        if (schedule == null) {
            LOG.warn("No schedule for Restaurant " + restaurantId);
            throw new EntityNotFoundException("There is no schedule for restaurant " + restaurantId);
        }

        OffsetDateTime clientTime = time.atOffset(ZoneOffset.UTC);

        LocalTime openHour, closeHour;

        openHour = (clientTime.getDayOfWeek().equals(DayOfWeek.SUNDAY) || clientTime.getDayOfWeek().equals(DayOfWeek.SATURDAY))
                ? schedule.getWeekendDaysOpeningHour() : schedule.getWorkDaysOpeningHour();
        closeHour = (clientTime.getDayOfWeek().equals(DayOfWeek.SUNDAY) || clientTime.getDayOfWeek().equals(DayOfWeek.SATURDAY))
                ? schedule.getWeekendDaysClosingHour() : schedule.getWorkDaysClosingHour();

        LocalDate localDate = clientTime.toLocalDate();
        LocalTime openTime = LocalTime.of(openHour.getHour(), openHour.getMinute());
        LocalDateTime openDate = LocalDateTime.of(localDate, openTime);

        LocalTime closeTime = LocalTime.of(closeHour.getHour(), closeHour.getMinute());
        LocalDateTime closeDate = LocalDateTime.of(localDate, closeTime);

        OffsetDateTime offsetOpen = OffsetDateTime.of(openDate, ZoneOffset.of(schedule.getUtcTimeZone()));
        OffsetDateTime offsetClose = OffsetDateTime.of(closeDate, ZoneOffset.of(schedule.getUtcTimeZone()));

        OffsetDateTime offsetOpenInUtc = offsetOpen.withOffsetSameInstant(ZoneOffset.UTC);
        OffsetDateTime offsetCloseInUtc = offsetClose.withOffsetSameInstant(ZoneOffset.UTC);

        return ((clientTime.compareTo(offsetCloseInUtc) == -1) && (clientTime.compareTo(offsetOpenInUtc) == 1)) ? "Open" : "Closed";
    }

}
