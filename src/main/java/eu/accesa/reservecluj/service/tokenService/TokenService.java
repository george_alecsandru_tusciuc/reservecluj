package eu.accesa.reservecluj.service.tokenService;

import java.util.Map;

public interface TokenService {

    String getJwkToken(Long id);

    String getJwkTokenWithRestaurant(Long member_id, Long restaurant_id);

    Map<String, Object> decryptToken(String token);
}
