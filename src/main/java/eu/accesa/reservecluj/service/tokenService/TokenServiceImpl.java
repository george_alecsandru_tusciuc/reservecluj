package eu.accesa.reservecluj.service.tokenService;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

@Service
public class TokenServiceImpl implements TokenService {

    @Value("${myConfig.secret}")
    private String secret;

    public String getJwkToken(Long id) {
        final String issuer = "https://accesa.eu/";
        final long iat = System.currentTimeMillis() / 1000l; // issued at claim
        final long exp = iat + 86400L; // expires claim. In this case the token expires in 60 seconds
        final JWTSigner signer = new JWTSigner(secret);
        final HashMap<String, Object> claims = new HashMap<String, Object>();
        claims.put("iss", issuer);
        claims.put("exp", exp);
        claims.put("iat", iat);
        claims.put("user_id", id);
        final String jwt = signer.sign(claims);
        return jwt;
    }

    public String getJwkTokenWithRestaurant(Long member_id, Long restaurant_id) {
        final String issuer = "https://accesa.eu/";
        final long iat = System.currentTimeMillis() / 1000l; // issued at claim
        final long exp = iat + 86400L; // expires claim. In this case the token expires in 60 seconds
        final JWTSigner signer = new JWTSigner(secret);
        final HashMap<String, Object> claims = new HashMap<String, Object>();
        claims.put("iss", issuer);
        claims.put("exp", exp);
        claims.put("iat", iat);
        claims.put("user_id", member_id);
        claims.put("restaurant_id", restaurant_id);
        final String jwt = signer.sign(claims);
        return jwt;
    }

    public Map<String, Object> decryptToken(String token) {
        try {
            final JWTVerifier verifier = new JWTVerifier(secret);
            final Map<String, Object> claims = verifier.verify(token);
            return claims;
        } catch (JWTVerifyException e) {
            throw new EntityNotFoundException("Invalid token indeed");
        } catch (NoSuchAlgorithmException e) {
            throw new EntityNotFoundException("Invalid token no such token");
        } catch (IOException e) {
            throw new EntityNotFoundException("Invalid token io");
        } catch (SignatureException e) {
            throw new EntityNotFoundException(e.toString());
        } catch (InvalidKeyException e) {
            throw new EntityNotFoundException("Invalid token invalid key");
        }
    }
}
