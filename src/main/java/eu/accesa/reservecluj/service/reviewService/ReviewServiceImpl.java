package eu.accesa.reservecluj.service.reviewService;

import eu.accesa.reservecluj.model.NoiseRating;
import eu.accesa.reservecluj.model.Review;
import eu.accesa.reservecluj.repo.ReviewRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Override
    public List<Review> findAll() {
        return reviewRepository.findAll();
    }

    @Override
    public Review findById(Long id) {
        return reviewRepository.findOne(id);
    }

    @Override
    public List<Review> findByDate(LocalDate date) {
        return reviewRepository.findByDate(date);
    }

    @Override
    public List<Review> findByRestaurantId(Long restaurantId) {
        return reviewRepository.findByRestaurantId(restaurantId);
    }

    @Override
    public void deleteAll() {
        reviewRepository.deleteAll();
    }

    @Override
    public void deleteOne(Long id) {
        reviewRepository.delete(id);
    }

    @Override
    public Review addReview(Review review) {
        return reviewRepository.save(review);
    }

    @Override
    public NoiseRating getOverallNoiseRatingForRestaurant(Long restaurantId) {
        Map<NoiseRating, Integer> map = new HashMap<>();
        for (NoiseRating s : NoiseRating.values()) {
            map.put(s, 0);
        }
        NoiseRating result = NoiseRating.LOW;
        int max = 0;
        for (Review r : reviewRepository.findByRestaurantId(restaurantId)) {
            map.put(r.getNoiseRating(), map.get(r.getNoiseRating()) + 1);
            if (map.get(r.getNoiseRating()) > max) {
                max = map.get(r.getNoiseRating());
                result = r.getNoiseRating();
            }
        }
        return result;
    }

    @Override
    public Double getReviewOverallRatingForReview(Review review) {
        Double mean = 0.0;
        mean += review.getAmbienceRating();
        mean += review.getFoodRating();
        mean += review.getServiceRating();
        mean += review.getValueRating();
        return mean / 4;
    }

    @Override
    public Double getOverAllRating(Long restaurantId, Function<Review , Double> function) {
        Double mean = 0.0;
        int nr = 0;
        for (Review r : reviewRepository.findByRestaurantId(restaurantId)) {
            mean += function.apply(r);
            ++nr;
        }
        return nr==0 ? 0 : mean / nr;
    }

    @Override
    public Review findByRestaurantAndReservation(Long restaurantId, Long reservationId) {
        return reviewRepository.findByRestaurantIdAndReservationId(restaurantId,reservationId);
    }
}
