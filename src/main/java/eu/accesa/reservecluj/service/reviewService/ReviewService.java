package eu.accesa.reservecluj.service.reviewService;

import eu.accesa.reservecluj.model.NoiseRating;
import eu.accesa.reservecluj.model.Review;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public interface ReviewService {
    List<Review> findAll();
    Review findById(Long id);
    List<Review> findByDate(LocalDate date);
    List<Review> findByRestaurantId (Long restaurantId);
    void deleteAll();
    void deleteOne(Long id);
    Review addReview(Review review);
    NoiseRating getOverallNoiseRatingForRestaurant(Long restaurantId);
    Double getReviewOverallRatingForReview(Review review);
    Double getOverAllRating(Long restaurantId, Function<Review , Double> function);
    Review findByRestaurantAndReservation(Long restaurantId, Long reservationId);
}
