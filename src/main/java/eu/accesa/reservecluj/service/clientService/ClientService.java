package eu.accesa.reservecluj.service.clientService;

import eu.accesa.reservecluj.model.Client;

import java.util.List;
import java.util.Map;

public interface ClientService {

    List<Client> getAllClients();

    Client getByEmail(String email);

    Client getOne(Long id);

    Client save(Client res);

    void delete(Long id);

    Client update(Long clientId, Client client);

    Client addClient(Client c);
}
