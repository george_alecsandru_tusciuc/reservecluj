package eu.accesa.reservecluj.service.clientService;

import eu.accesa.reservecluj.model.Client;
import eu.accesa.reservecluj.repo.ClientRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    public ClientRepository clientRepository;

    private static final Logger LOG = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Override
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public Client getByEmail(String email) {
        return clientRepository.findByEmail(email);
    }

    @Override
    public Client getOne(Long id) {
        return clientRepository.findOne(id);
    }

    @Override
    public Client save(Client res) {
        return clientRepository.save(res);
    }

    @Override
    public void delete(Long id) {
         clientRepository.delete(id);
    }

    @Override
    public Client addClient(Client cli) {
        Client c1 = clientRepository.findByEmail(cli.getEmail());
        if (c1 == null) {
            return clientRepository.save(cli);
        }
        return c1;
    }

    @Override
    public Client update(Long clientId, Client client){
        Client c1 = clientRepository.findOne(clientId);
        if (c1 == null) {
            LOG.warn("there is no client with this id ");
            throw new EntityNotFoundException("There is no client with this id");
        }
        c1.setName(client.getName());
        c1.setEmail(client.getEmail());
        clientRepository.save(c1);
        return c1;

    }
}
