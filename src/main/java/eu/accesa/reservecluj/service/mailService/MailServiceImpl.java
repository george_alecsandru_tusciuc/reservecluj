package eu.accesa.reservecluj.service.mailService;

import eu.accesa.reservecluj.model.Reservation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailServiceImpl implements MailService {

    private static final Logger LOG = LoggerFactory.getLogger(MailServiceImpl.class);

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private JavaMailSender mailSender;

    private Context setGeneralContext(Reservation reservation, String User) {
        Context context1 = new Context();
        context1.setVariable("user", User);
        context1.setVariable("hour", reservation.getHour());
        context1.setVariable("restaurant", reservation.getRestaurant().getName());
        context1.setVariable("time", reservation.getHour().getHour() < 12 ? "AM" : "PM");
        return context1;
    }

    @Override
    public String reviewEmailBuild(Reservation reservation,String User, String template,String Link) {
        Context context = setGeneralContext(reservation, User);
        context.setVariable("reviewLink", Link);
        return templateEngine.process(template, context);
    }

    @Override
    public String confirmationEmailBuild(String message, Reservation reservation, String User, String template) {
        Context context = setGeneralContext(reservation, User);
        context.setVariable("status", reservation.getReservationStatus().toString().toLowerCase());
        context.setVariable("message", message);
        return templateEngine.process(template, context);
    }

    @Override
    public String sendEmail(String recipient, String subject, String content ) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setFrom("reservationsCluj@gmail.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject(subject);
            messageHelper.setText(content,true);
        };
        mailSender.send(messagePreparator);
        return "Email was sent";
    }
}