package eu.accesa.reservecluj.service.mailService;

import eu.accesa.reservecluj.model.Reservation;

public interface MailService {

    String reviewEmailBuild(Reservation reservation,String User, String template,String Link);

    String confirmationEmailBuild(String message, Reservation reservation, String User, String template);

    String sendEmail(String recipient, String content, String subject);
}
