package eu.accesa.reservecluj.service.restaurantService;

import eu.accesa.reservecluj.controller.dto.RestaurantDto;
import eu.accesa.reservecluj.model.Restaurant;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public interface RestaurantService {
    List<Restaurant> getAllRestaurants();

    Restaurant getRestaurantById(Long id);

    Restaurant save(Long restaurantId, RestaurantDto restaurantDto);

    Restaurant insertRestaurant(Restaurant restaurant);

    Restaurant update(Restaurant restaurant, Long id);

    List<Restaurant> sort(BiFunction<Restaurant ,Restaurant, Integer> function);
}
