package eu.accesa.reservecluj.service.restaurantService;


import eu.accesa.reservecluj.controller.dto.RestaurantDto;
import eu.accesa.reservecluj.model.Feature;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.repo.RestaurantRepository;
import eu.accesa.reservecluj.service.featureService.RestaurantFeatureService;
import eu.accesa.reservecluj.service.memberService.MemberService;
import eu.accesa.reservecluj.service.reservationService.ReservationService;
import eu.accesa.reservecluj.service.reviewService.ReviewService;
import eu.accesa.reservecluj.service.tokenService.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private MemberService memberService;

    @Autowired
    private RestaurantFeatureService featureService;

    @Autowired
    TokenService tokenService;

    private static final Logger LOG = LoggerFactory.getLogger(RestaurantServiceImpl.class);

    @Override
    public List<Restaurant> getAllRestaurants() {
        return restaurantRepository.findAll();
    }

    @Override
    public Restaurant save(Long restaurantId, RestaurantDto restaurantDto) {
        Restaurant restaurant = restaurantRepository.findOne(restaurantId);

        List<Restaurant> dummyList = new ArrayList<>();
        dummyList.add(restaurant);
        for (Feature f : restaurant.getFeatures()) {
            if (!restaurantDto.getFeatures().contains(f)) ;
            featureService.deleteRestaurantFeature(f.getId(), restaurantId);
        }

        for (Feature f : restaurantDto.getFeatures()) {
            f.setRestaurants(dummyList);
            featureService.addRestaurantFeature(f);
        }

        restaurant.setFeatures(restaurantDto.getFeatures());
        restaurant.setLinkSocial(restaurantDto.getLinkSocial());
        restaurant.setName(restaurantDto.getName());
        restaurant.setDescription(restaurantDto.getDescription());
        return restaurantRepository.save(restaurant);
    }

    public Restaurant insertRestaurant(Restaurant restaurant) {
        return restaurantRepository.save(restaurant);
    }

    public Restaurant update(Restaurant rest, Long id) {
        Restaurant r1 = restaurantRepository.findOne(id);
        if (r1 != null) {
            r1.setName(rest.getName());
            r1.setDescription(rest.getDescription());
            return r1;
        }
        LOG.warn("There is no restaurant with that id");
        throw new EntityNotFoundException("There is no restaurant with that id");
    }

    @Override
    public List<Restaurant> sort(BiFunction<Restaurant, Restaurant, Integer> function) {
        List<Restaurant> restaurants = restaurantRepository.findAll();
        Collections.sort(restaurants, function::apply);
        return restaurants.subList(0, restaurants.size() > 6 ? 6 : restaurants.size());
    }

    @Override
    public Restaurant getRestaurantById(Long id) {
        return restaurantRepository.findOne(id);
    }
}
