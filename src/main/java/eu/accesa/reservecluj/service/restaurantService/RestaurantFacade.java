package eu.accesa.reservecluj.service.restaurantService;

import eu.accesa.reservecluj.controller.dto.RestaurantDto;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.service.addressService.AddressService;
import eu.accesa.reservecluj.service.featureService.RestaurantFeatureService;
import eu.accesa.reservecluj.service.pictureService.PictureService;
import eu.accesa.reservecluj.service.reservationService.ReservationService;
import eu.accesa.reservecluj.service.reservationService.ReservationService;
import eu.accesa.reservecluj.service.reviewService.ReviewService;
import eu.accesa.reservecluj.service.scheduleService.ScheduleService;
import eu.accesa.reservecluj.service.solrService.SolrService;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RestaurantFacade {

    @Autowired
    private ReviewService reviewService;
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private PictureService pictureService;
    @Autowired
    private SolrService solrService;
    @Autowired
    private RestaurantFeatureService featureService;
    @Autowired
    private ReservationService reservationService;

    public void updateRestaurant(Long restaurantId, RestaurantDto restaurantDto) throws IOException, SolrServerException {

        solrService.addIndex(restaurantDto);

        scheduleService.updateSchedule(restaurantDto.getSchedule(), restaurantId);

        addressService.changeAddressForRestaurant(restaurantId, restaurantDto.getAddress());

        restaurantService.save(restaurantId, restaurantDto);
    }

    public List<RestaurantDto> solrSearch(String query) throws SolrServerException {
        return solrService.solrSearch(query);
    }

    public List<Restaurant> getAllRestaurants() {
        return restaurantService.getAllRestaurants();
    }

    public Restaurant getRestaurantById(Long restaurantId) {
        return restaurantService.getRestaurantById(restaurantId);
    }

    public Restaurant insertRestaurant(Restaurant restaurant) {
        return restaurantService.insertRestaurant(restaurant);
    }

    public List<Restaurant> getFavoritesForUser(String userEmail) {
        List<Restaurant> restaurantList = restaurantService.getAllRestaurants();
        Map<Long, Long>restaurantMap = restaurantList.stream()
                .collect(Collectors.toMap(Restaurant::getId,
                        e->reservationService.countByEmailAndRestaurantId(userEmail, e.getId())));
        return restaurantList
                .stream()
                .sorted((x,x1)->Long.compare(
                        restaurantMap.get(x1.getId()),
                        restaurantMap.get(x.getId())))
                .limit(3)
                .collect(Collectors.toList());
    }

    public List<Restaurant> sortByRating() {
        return restaurantService.sort((r, r1) -> reviewService.getOverAllRating(r1.getId(), reviewService::getReviewOverallRatingForReview).compareTo(reviewService.getOverAllRating(r.getId(), reviewService::getReviewOverallRatingForReview)));
    }

    public List<Restaurant> sortByNrOfVisits() {
        return restaurantService.sort((r, r1) -> reservationService.findByRestaurant(r1).size() - reservationService.findByRestaurant(r).size());
    }
}
