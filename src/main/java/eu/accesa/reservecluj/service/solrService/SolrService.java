package eu.accesa.reservecluj.service.solrService;

import eu.accesa.reservecluj.controller.dto.RestaurantDto;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;
import java.util.List;

public interface SolrService {
    void deleteAllIndexes() throws IOException, SolrServerException;

    void addAllIndexes() throws IOException, SolrServerException;

    List<RestaurantDto> solrSearch(String query) throws SolrServerException;

    void addIndex(RestaurantDto restaurantDto) throws IOException, SolrServerException;

    void deleteIndex(Long id) throws IOException, SolrServerException;
}
