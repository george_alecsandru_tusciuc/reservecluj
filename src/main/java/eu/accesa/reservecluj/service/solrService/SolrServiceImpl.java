package eu.accesa.reservecluj.service.solrService;

import eu.accesa.reservecluj.controller.dto.RestaurantDto;
import eu.accesa.reservecluj.converter.SolrDocumentRestaurantDtoConverter;
import eu.accesa.reservecluj.model.Address;
import eu.accesa.reservecluj.model.Feature;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.model.Schedule;
import eu.accesa.reservecluj.service.addressService.AddressService;
import eu.accesa.reservecluj.service.featureService.RestaurantFeatureService;
import eu.accesa.reservecluj.service.restaurantService.RestaurantService;
import eu.accesa.reservecluj.service.scheduleService.ScheduleService;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SolrServiceImpl implements SolrService {

    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private AddressService addressService;
    @Autowired
    RestaurantFeatureService restaurantFeatureService;
    @Autowired
    private SolrServer solrServer;

    @Override
    public void deleteAllIndexes() throws IOException, SolrServerException {
        solrServer.deleteByQuery("*:*");
        solrServer.commit();
    }

    @Override
    public void addAllIndexes() throws IOException, SolrServerException {
        for (Restaurant r : restaurantService.getAllRestaurants()) {
            SolrInputDocument solrInputDocument = new SolrInputDocument();
            solrInputDocument.addField("id", r.getId().toString());
            solrInputDocument.addField("description", r.getDescription());
            solrInputDocument.addField("name", r.getName());
            solrInputDocument.addField("linkSocial", r.getLinkSocial());
            List<String> featureNames = r.getFeatures().stream()
                    .map(Feature::getName)
                    .collect(Collectors.toList());
            solrInputDocument.addField("features", featureNames);
            Schedule schedule = scheduleService.findByRestaurant(r.getId());
            if (schedule != null) {
                solrInputDocument.addField("weekend_days_closing_hour", schedule.getWeekendDaysClosingHour().toString());
                solrInputDocument.addField("weekend_days_opening_hour", schedule.getWeekendDaysOpeningHour().toString());
                solrInputDocument.addField("working_days_opening_hour", schedule.getWorkDaysOpeningHour().toString());
                solrInputDocument.addField("working_days_closing_hour", schedule.getWorkDaysClosingHour().toString());
            }
            Address address = addressService.findAddressForRestaurant(r.getId());
            if (address != null) {
                solrInputDocument.addField("street", address.getStreet());
                solrInputDocument.addField("longitude", address.getLongitude().toString());
                solrInputDocument.addField("latitude", address.getLatitude().toString());
            }
            solrServer.add(solrInputDocument);
        }
        solrServer.commit();
    }

    @Override
    public List<RestaurantDto> solrSearch(String query) throws SolrServerException {
        ModifiableSolrParams params = new ModifiableSolrParams();
        List<String> queries = Arrays.asList(query.split(" "));
        String queryParam = queries.stream()
                .map(x -> " text:*" + x + "*")
                .collect(Collectors.joining());
        params.add("q", queryParam);
        params.add("rows","20");
        SolrDocumentRestaurantDtoConverter solrDocumentRestaurantDtoConverter = new SolrDocumentRestaurantDtoConverter();
        QueryResponse queryResponse = solrServer.query(params);
        SolrDocumentList restaurants = queryResponse.getResults();
        return restaurants.stream()
                .map(solrDocumentRestaurantDtoConverter::convertDocumentToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void addIndex(RestaurantDto restaurantDto) throws IOException, SolrServerException {
        SolrDocumentRestaurantDtoConverter solrDocumentRestaurantDtoConverter = new SolrDocumentRestaurantDtoConverter();
        solrServer.add(solrDocumentRestaurantDtoConverter.convertDtoToDocument(restaurantDto));
        solrServer.commit();
    }

    @Override
    public void deleteIndex(Long id) throws IOException, SolrServerException {
        solrServer.deleteByQuery("id:" + id);
        solrServer.commit();
    }

}
