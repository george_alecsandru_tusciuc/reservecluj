package eu.accesa.reservecluj.service.security;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuthorizationAdvice {

    private static Logger logger = LoggerFactory.getLogger(AuthorizationAdvice.class);

    @Around("execution(@ReserveSecured * *.*(..))")
    public Object before(ProceedingJoinPoint joinpoint) {
        try {
            ReserveSecured annotation = ((MethodSignature) joinpoint.getSignature()).getMethod()
                    .getAnnotation(ReserveSecured.class);
            if (annotation.isIdFirstParam()) {
                Long userId = (Long) joinpoint.getArgs()[0];
                SecurityContext sec = SecurityContextHolder.getContext();
                RestaurantAuthentication auth = (RestaurantAuthentication) sec.getAuthentication();
                Long resId = auth.getRestaurantId();
                if (resId != userId) {
                    throw new SecurityException("don't have rights for this operation");
                }
            }
            return joinpoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            throw new RuntimeException(throwable);
        }
    }
}
