package eu.accesa.reservecluj.service.security;

import eu.accesa.reservecluj.service.tokenService.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

public class RestaurantAccessFilter implements Filter {
    public static final String filtru = "Authorization";
    @Autowired
    public TokenService tokenService;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        String header = request.getHeader("Authorization");
        Map<String, Object> token = tokenService.decryptToken(header);
        SecurityContext sec = SecurityContextHolder.getContext();
        RestaurantAuthentication aut = new RestaurantAuthentication();
        Long reId;
        try {
            reId = (Long) token.get("user_id");
        } catch (Exception e) {
            Integer us = (Integer) token.get("user_id");
            reId = Long.valueOf(us);
        }
        aut.setUserId(reId);
        sec.setAuthentication(aut);
        chain.doFilter(req, res);
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig asrg0) throws ServletException {
    }
}
