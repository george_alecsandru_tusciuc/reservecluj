package eu.accesa.reservecluj.service.security;

import org.springframework.security.Authentication;
import org.springframework.security.GrantedAuthority;

public class RestaurantAuthentication implements Authentication {

    Long userId;

    Long restaurantId;

    @Override
    public GrantedAuthority[] getAuthorities() {
        return new GrantedAuthority[0];
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
    }

    @Override
    public String getName() {
        return null;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
