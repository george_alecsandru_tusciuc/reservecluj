package eu.accesa.reservecluj.service.security;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ReserveSecured {
    boolean isIdFirstParam() default false;
}
