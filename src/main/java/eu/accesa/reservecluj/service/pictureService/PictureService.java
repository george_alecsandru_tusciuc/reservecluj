package eu.accesa.reservecluj.service.pictureService;

import eu.accesa.reservecluj.model.Picture;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

public interface PictureService {

    List<Picture> findByRestaurant(Long id);

    List<Picture> findAll();
    
    Picture save(Picture picture);

    Picture findOne(Long id);

    Picture uploadImage(Long restaurantId, MultipartFile file, RedirectAttributes redirectAttributes);

    void delete(Long restaurantId, Long pictureId);

    Picture addImage(Picture picture);
}
