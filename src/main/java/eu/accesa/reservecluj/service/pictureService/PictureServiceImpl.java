package eu.accesa.reservecluj.service.pictureService;

import eu.accesa.reservecluj.model.Picture;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.repo.PictureRepository;
import eu.accesa.reservecluj.repo.RestaurantRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;


@Service
public class PictureServiceImpl implements PictureService {

    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private RestaurantRepository restaurantRepository;

    @Value("${image.path}")
    private String imagePath;

    public static String getRandomPictureName() {
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }

    private static String getFileExtension(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf("."));
        else return "";
    }

    @Override
    public List<Picture> findByRestaurant(Long id) {
        return pictureRepository.findByRestaurant(restaurantRepository.findOne(id));
    }

    @Override
    public List<Picture> findAll() {

        return pictureRepository.findAll();
    }

    @Override
    public Picture save(Picture picture) {
        return pictureRepository.save(picture);
    }

    @Override
    public Picture findOne(Long id) {
        return pictureRepository.findOne(id);
    }

    /**
     * Uploads a picture and stores it on the disk on a specified path.
     * @param restaurantId  The id of the restaurant that will hold the picture
     * @param file          The picture itself
     * @param redirectAttributes  The redirect attributes that will display the succes or failure of the upload
     * @return              The picture that has been uploaded
     */
    @Override
    public Picture uploadImage(Long restaurantId, MultipartFile file, RedirectAttributes redirectAttributes) {
        Picture picture = new Picture();
        picture.setRestaurant(restaurantRepository.findOne(restaurantId));
        if (!file.isEmpty()) {
            try {
                String directoryPath = imagePath + File.separator + picture.getRestaurant().getId();
                makeDir(directoryPath);
                String filePath = directoryPath + File.separator + PictureServiceImpl.getRandomPictureName() + getFileExtension(file);
                File destination = new File(filePath);
                file.transferTo(destination);
                picture.setPath(filePath);
                pictureRepository.save(picture);
                redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + file.getOriginalFilename() + "!");
            } catch (IOException | RuntimeException e) {
                redirectAttributes.addFlashAttribute("message", "Failued to upload " + file.getOriginalFilename() + " => " + e.getMessage());
            }
        } else {
            redirectAttributes.addFlashAttribute("message", "Failed to upload " + file.getOriginalFilename() + " because it was empty");
        }
        return picture;
    }

    @Override
    public void delete(Long restaurantId, Long pictureId) {
        Picture picture = pictureRepository.findOne(pictureId);
        Restaurant restaurant = restaurantRepository.findOne(restaurantId);
        if (picture.getRestaurant().getId() == restaurant.getId()) {
            String path = picture.getPath();
            deleteFile(path);
            pictureRepository.delete(pictureId);
        } else {
            throw new RuntimeException("Unable to delete entry in the database");
        }
    }
    
    @Override
    public Picture addImage(Picture picture) {
        return pictureRepository.save(picture);
    }
    
    
    private void deleteFile(String path) {
        File file = new File(path);
        if (file.delete()) {
            System.out.println(file.getName() + " is deleted!");
        } else {
            System.out.println("Delete operation failed.");
        }
    }

    private void makeDir(String filePath) {
        File dest = new File(filePath);
        if (!dest.exists()) {
            if (dest.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
    }

}