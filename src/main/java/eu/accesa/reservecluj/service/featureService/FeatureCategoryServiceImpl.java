package eu.accesa.reservecluj.service.featureService;

import eu.accesa.reservecluj.model.FeatureCategory;
import eu.accesa.reservecluj.repo.FeatureCategoryRepository;
import org.hibernate.metamodel.relational.IllegalIdentifierException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeatureCategoryServiceImpl implements FeatureCategoryService {

    @Autowired
    FeatureCategoryRepository featureCategoryRepository;

    private static final Logger LOG = LoggerFactory.getLogger(FeatureCategoryServiceImpl.class);

    @Override
    public FeatureCategory addFeatureCategory(FeatureCategory featureCategory) throws IllegalArgumentException {
        if (featureCategoryRepository.findByNameIgnoreCase(featureCategory.getName()).size() > 0) {
            LOG.warn("Duplicate feature category insertion!");
            throw new IllegalArgumentException("Data already Exists");
        }
        return featureCategoryRepository.save(featureCategory);
    }

    @Override
    public FeatureCategory getFeatureCategoryByName(String name) throws IllegalIdentifierException {
        LOG.info(name);
        List<FeatureCategory> fc = featureCategoryRepository.findByNameIgnoreCase(name);
        if (fc.size() > 0) return fc.get(0);
        LOG.warn("NonExistent cetegory query!");
        throw new IllegalIdentifierException("No such category name.");
    }

    @Override
    public void deleteFeatureCategory(long categoryId) throws IllegalIdentifierException {
        if (featureCategoryRepository.findOne(categoryId) == null) {
            LOG.warn("Deletion request of NonExistent featureCategory");
            throw new IllegalIdentifierException("No such category");
        }
        featureCategoryRepository.delete(categoryId);
    }

    @Override
    public List<FeatureCategory> getCategories() {
        return featureCategoryRepository.findAll();
    }
}