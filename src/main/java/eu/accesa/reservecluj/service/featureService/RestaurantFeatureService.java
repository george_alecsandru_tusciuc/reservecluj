package eu.accesa.reservecluj.service.featureService;

import eu.accesa.reservecluj.controller.dto.RestaurantFeatureSearchDTO;
import eu.accesa.reservecluj.model.Feature;
import eu.accesa.reservecluj.model.Restaurant;
import org.hibernate.metamodel.relational.IllegalIdentifierException;

import java.util.List;

public interface RestaurantFeatureService {

    List<Feature> getAllFeatures();

    List<Restaurant> filterRestaurants(RestaurantFeatureSearchDTO features);

    Feature addRestaurantFeature(Feature feature) throws IllegalArgumentException;

    void deleteRestaurantFeature(long featureId, long restaurantId) throws IllegalIdentifierException;

    List<Feature> getFeaturesByRestaurantId(long restaurantId) throws IllegalIdentifierException;
}
