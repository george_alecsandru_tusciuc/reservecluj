package eu.accesa.reservecluj.service.featureService;

import eu.accesa.reservecluj.controller.dto.RestaurantFeatureSearchDTO;
import eu.accesa.reservecluj.model.Feature;
import eu.accesa.reservecluj.model.Feature_;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.model.Restaurant_;
import eu.accesa.reservecluj.repo.FeatureRepository;
import eu.accesa.reservecluj.repo.RestaurantRepository;

import org.hibernate.metamodel.relational.IllegalIdentifierException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Service
public class RestaurantFeatureServiceImpl implements RestaurantFeatureService {

    private static final Logger LOG = LoggerFactory.getLogger(RestaurantFeatureServiceImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private FeatureRepository featureRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Override
    public List<Feature> getAllFeatures() {
        return featureRepository.findAll();
    }

    @Override
    public List<Restaurant> filterRestaurants(RestaurantFeatureSearchDTO features) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Restaurant> cq = cb.createQuery(Restaurant.class);

        Root<Restaurant> from = cq.from(Restaurant.class);
        ListJoin<Restaurant, Feature> featuresRoot = from.join(Restaurant_.features);
        cq.groupBy(from.get(Restaurant_.id));
        List<Predicate> predicates = new ArrayList<>();
        if (!features.getQuisine().isEmpty())
            predicates.add(cb.gt(cb.count(cb.selectCase().when(featuresRoot.get(Feature_.name).in(features.getQuisine()), 1).otherwise(cb.nullLiteral(Number.class))), 0));
        if (!features.getGeneral().isEmpty()) {
            predicates.addAll(features.getGeneral().stream().map(generalFeature -> cb.gt(cb.count(cb.selectCase().when(cb.equal(featuresRoot.get(Feature_.name), generalFeature), 1).otherwise(cb.nullLiteral(Number.class))), 0)).collect(Collectors.toList()));
        }
        cq.having(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        TypedQuery<Restaurant> query = em.createQuery(cq);

        return query.getResultList();
    }

    @Override
    public Feature addRestaurantFeature(Feature feature) {
        Feature exists = featureRepository.findOneByName(feature.getName());
        if (exists != null) {
            if (exists.getRestaurants().contains(feature.getRestaurants().get(0))) {
                //LOG.warn("Duplicate property insertion!");
                //throw new IllegalArgumentException("Property already exists for specified Restaurant");
                return exists;
            }
            exists.getRestaurants().add(feature.getRestaurants().get(0));
            return featureRepository.save(exists);
        }
        return featureRepository.save(feature);
    }

    @Override
    public void deleteRestaurantFeature(long featureId, long restaurantId) throws IllegalIdentifierException {
        Feature exists = featureRepository.findOne(featureId);
        Restaurant restaurant = restaurantRepository.findOne(restaurantId);
        if (exists != null) {
            if (exists.getRestaurants().contains(restaurant)) {
                exists.getRestaurants().remove(restaurant);
                featureRepository.save(exists);
                return;
            } else
                LOG.warn("Query for nonexistent feature-restaurant combination!");
            throw new IllegalIdentifierException("No such feature for specified Restaurant.");

        }
        LOG.warn("Nonexistent feature query!");
        throw new IllegalIdentifierException("No such feature.");
    }

    @Override
    public List<Feature> getFeaturesByRestaurantId(long restaurantId) throws IllegalIdentifierException {
        Restaurant restaurant = restaurantRepository.findOne(restaurantId);
        if (restaurant != null) {
            return restaurant.getFeatures();
        }
        LOG.warn("Nonexistent restaurant query!");
        throw new IllegalIdentifierException("No such restaurant");
    }
}
