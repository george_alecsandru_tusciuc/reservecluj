package eu.accesa.reservecluj.service.featureService;


import eu.accesa.reservecluj.model.FeatureCategory;
import org.hibernate.metamodel.relational.IllegalIdentifierException;

import java.util.List;

public interface FeatureCategoryService {

    FeatureCategory addFeatureCategory(FeatureCategory featureCategory) throws IllegalArgumentException;

    FeatureCategory getFeatureCategoryByName(String name) throws IllegalIdentifierException;

    void deleteFeatureCategory(long categoryId) throws IllegalIdentifierException;

    List<FeatureCategory> getCategories();
}
