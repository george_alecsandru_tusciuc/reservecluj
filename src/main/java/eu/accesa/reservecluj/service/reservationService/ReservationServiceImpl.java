package eu.accesa.reservecluj.service.reservationService;

import eu.accesa.reservecluj.model.Reservation;
import eu.accesa.reservecluj.model.ReservationStatus;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.repo.ReservationRepository;
import eu.accesa.reservecluj.repo.RestaurantRepository;
import eu.accesa.reservecluj.service.mailService.MailService;

import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService {

    private static final Logger LOG = LoggerFactory.getLogger(ReservationServiceImpl.class);

    private ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    @Autowired
    private ReservationRepository repository;
    @Autowired
    private MailService mailService;
    @Autowired
    private RestaurantRepository restaurantRepository;
    @Autowired
    private ApplicationContext appContext;
    @Value("${deployment.path}")
    private String deploymentPath;

    @Override
    public String sendConfirmationMail(String message, String recipient, String subject, Reservation reservation, String template) throws ExecutionException, InterruptedException {
        return sendEmail(recipient, subject, () -> mailService.confirmationEmailBuild(message, reservation, recipient, template));
    }

    @Override
    public String sendReviewMail(String recipient, String subject, Reservation reservation, String template) throws ExecutionException, InterruptedException {
        String Link=deploymentPath+appContext.getApplicationName()+"#restaurant/"+reservation.getRestaurant().getId()
                +"/reservation/"+reservation.getId();
        return sendEmail(recipient, subject, () -> mailService.reviewEmailBuild(reservation, recipient, template,Link));
    }

    private String sendEmail(String recipient, String subject, Callable<String> callable) {
        try {
            Future<String> result = executorService.submit(() -> mailService.sendEmail(recipient, subject, callable.call()));
            LOG.info(result.get());
        } catch (ExecutionException | InterruptedException exception) {
            String message = "mail was not sent" + "\n" + exception.getMessage();
            LOG.error(message);
            throw new RuntimeException(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Email was sent";
    }

    @Override
    public void updateStatus(Reservation r, String status) {
        if (EnumUtils.isValidEnum(ReservationStatus.class, status)) {
            r.setReservationStatus(ReservationStatus.valueOf(status));
            repository.save(r);
        }
    }

    @Override
    public List<Reservation> findAll() {
        return repository.findAll();
    }

    @Override
    public void addReservation(Long reservationId, Reservation r) {
        r.setRestaurant(restaurantRepository.findOne(reservationId));
        repository.save(r);
    }

    @Override
    public void addReservations(List<Reservation> reservations) {
        repository.save(reservations);
    }

    @Override
    public Reservation findOne(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Reservation> findByRestaurant(Restaurant restaurant) {
        return repository.findByRestaurant(restaurant);
    }

    @Override
    public List<Reservation> findByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public List<Reservation> findByEmailOrderedByDateAscFirst10(String email) {
        return repository.findFirst10ByEmailOrderByDateDesc(email);
    }


    @Override
    public List<Reservation> findByDate(LocalDate date) {
        return repository.findByDate(date);
    }

    @Override
    public List<Reservation> findByHour(int hour) {
        return repository.findByHour(hour);
    }

    @Override
    public List<Reservation> findByNrPersons(int nrPersons) {
        return repository.findByNrPersons(nrPersons);
    }

    @Override
    public List<Reservation> findByReservationStatus(ReservationStatus reservationStatus) {
        return repository.findByReservationStatus(reservationStatus);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public void delete(List<? extends Reservation> reservations) {
        repository.delete(reservations);
    }

    @Override
    public void delete(Reservation reservation) {
        repository.delete(reservation);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Long countByEmailAndRestaurantId(String email, Long restaurantId) {
        return repository.countByEmailAndRestaurantId(email, restaurantId);
    }

    @Override
    public Reservation findByIdandRestaurantId(Long Id, Long restaurantId) {
        return repository.findByIdAndRestaurantId(Id,restaurantId);
    }

    public ReservationRepository getRepository() {
        return repository;
    }

    public void setRepository(ReservationRepository repository) {
        this.repository = repository;
    }
}
