package eu.accesa.reservecluj.service.reservationService;

import eu.accesa.reservecluj.model.Reservation;
import eu.accesa.reservecluj.model.ReservationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.core.Local;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;

@Component
public class ReservationMaintenance {
    private static final Logger LOGGER = Logger.getLogger("myLog");
    @Autowired
    public ReservationService reservationService;
    @Value("${log.path}")
    private String logPath;

    @Scheduled(cron = "${mailReview.cron.expression}")
    private void reservationStatusUpdate() {
        for (Reservation r : reservationService.findByReservationStatus(ReservationStatus.NEW)) {
            reservationService.updateStatus(r, ReservationStatus.NO_RESPONSE.toString());
        }
    }

    @Scheduled(cron = "${cron.expression}")
    private void jobStatusUpdate() {
        List<Reservation> reservationList = reservationService.findByReservationStatus(ReservationStatus.PENDING);

        try {
            SimpleFormatter formatter = new SimpleFormatter();
            FileHandler fh = new FileHandler(logPath + File.separator + getFileName());
            fh.setFormatter(formatter);
            LOGGER.addHandler(fh);
            for (Reservation r : reservationList) {
                LOGGER.info(r.toString());
                reservationService.updateStatus(r, ReservationStatus.NOT_PROCESSED.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "${mailReview.cron.expression}")
    private void reviewMailSend() {
        List<Reservation> reservationList = reservationService.findByReservationStatus(ReservationStatus.DONE);
        reservationList.addAll(reservationService.findByReservationStatus(ReservationStatus.CONFIRMED));

        List<Reservation> yesterdaysReservations = reservationList.stream().filter(
                x -> x.getDate().isBefore(LocalDate.now()) && x.getDate().isAfter(LocalDate.now().minusDays(2))
        ).collect(Collectors.toList());

        for (Reservation r : yesterdaysReservations) {
            try {
                reservationService.sendReviewMail(r.getEmail(),"Reservation review", r, "review");
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String getFileName() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
        Date date = new Date();
        String logFileName = dateFormat.format(date);
        return logFileName + ".log";
    }
}
