package eu.accesa.reservecluj.service.reservationService;

import eu.accesa.reservecluj.model.Reservation;
import eu.accesa.reservecluj.model.ReservationStatus;
import eu.accesa.reservecluj.model.Restaurant;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ExecutionException;

public interface ReservationService {

    String sendConfirmationMail(String message, String recipient, String subject, Reservation reservation, String template) throws ExecutionException, InterruptedException;

    String sendReviewMail(String recipient,String subject, Reservation reservation, String template) throws ExecutionException, InterruptedException;

    void updateStatus(Reservation r, String status);

    List<Reservation> findAll();

    void addReservation(Long restaurantId,Reservation r);

    void addReservations(List<Reservation> reservations);

    Reservation findOne(Long id);

    List<Reservation> findByRestaurant(Restaurant restaurant);

    List<Reservation> findByEmail(String email);

    List<Reservation> findByEmailOrderedByDateAscFirst10(String email);

    List<Reservation> findByDate(LocalDate date);

    List<Reservation> findByHour(int hour);

    List<Reservation> findByNrPersons(int nrPersons);

    List<Reservation> findByReservationStatus(ReservationStatus status);

    void deleteAll();

    void delete(List<? extends Reservation> reservations);

    void delete(Reservation reservation);

    void delete(Long id);

    Long countByEmailAndRestaurantId(String email, Long restaurantId);

    Reservation findByIdandRestaurantId(Long Id,Long restaurantId);
}
