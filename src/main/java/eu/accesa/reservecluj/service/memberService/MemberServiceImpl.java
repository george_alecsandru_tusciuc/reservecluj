package eu.accesa.reservecluj.service.memberService;

import eu.accesa.reservecluj.model.Member;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.repo.MemberRepository;
import eu.accesa.reservecluj.repo.RestaurantRepository;
import eu.accesa.reservecluj.service.tokenService.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MemberServiceImpl implements MemberService {

    private static MessageDigest md;

    @Autowired
    public RestaurantRepository restaurantRepository;

    @Autowired
    public TokenService tokenService;

    @Autowired
    private MemberRepository memberRepository;

    private static final Logger LOG = LoggerFactory.getLogger(MemberServiceImpl.class);

    private static String cryptWithMD5(String pass) {
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] passBytes = pass.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < digested.length; i++) {
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            LOG.warn("no algorithm for cryptation");
            throw new EntityNotFoundException("Try again");
        }
    }

    @Override
    public List<Member> getAllMembers() {
        return memberRepository.findAll();
    }

    @Override
    public Member getByRestaurant(Restaurant restaurant) {
        return memberRepository.findByRestaurant(restaurant);
    }

    @Override
    public Member getOne(Long id) {
        Member m1 = memberRepository.findOne(id);
        if (m1 != null) {
            m1.setPassword("");
            return m1;
        }
        LOG.warn("There is no member with this id");
        throw new EntityNotFoundException("There is no a member with this id");
    }

    @Override
    public Member save(Member mem) {
        return memberRepository.save(mem);
    }

    @Override
    public void delete(Long id) {
        memberRepository.delete(id);
    }

    public Member addMember(Member mem) {
        Member m1 = memberRepository.findByUsername(mem.getUsername());
        if (m1 == null) {
            String pass = mem.getPassword();
            pass = cryptWithMD5(pass);
            mem.setPassword(pass);
            return memberRepository.save(mem);
        }
        LOG.warn("There is already a member with this username");
        throw new EntityExistsException("There is already a member with this username");
    }

    public String assocMemberRestaurant(String token, Long restaurant_id) {
        Map<String, Object> ms = tokenService.decryptToken(token);
        Long userId;
        try {
            userId = (Long) ms.get("user_id");
        } catch (Exception e) {
            Integer us = (Integer) ms.get("user_id");
            userId = Long.valueOf(us);
        }
        Member m = memberRepository.findOne(userId);
        if (m.getRestaurant() != null) {
            throw new EntityExistsException("you already have a restaurant on this account");
        }
        if (m != null) {
            Restaurant res = restaurantRepository.findOne(restaurant_id);
            if (res != null) {
                m.setRestaurant(res);
                memberRepository.save(m);
                return tokenService.getJwkTokenWithRestaurant(userId, restaurant_id);
            }
        }
        LOG.warn("Illegal arguments");
        throw new EntityNotFoundException("Illegal arguments");
    }

    public Map<String, String> loginMember(Member mem) {
        Member m1 = memberRepository.findByUsername(mem.getUsername());
        Map<String, String> response = new HashMap<>();
        String jwkToken;
        if (m1 != null) {
            String pass1 = m1.getPassword();
            String pass2 = mem.getPassword();
            pass2 = cryptWithMD5(pass2);
            response.put("user_id", m1.getId().toString());
            if (pass1.equals(pass2)) {
                Restaurant res = m1.getRestaurant();
                if (res != null) {
                    jwkToken = tokenService.getJwkTokenWithRestaurant(m1.getId(), res.getId());
                    response.put("token", jwkToken);
                    response.put("restaurant_id", m1.getRestaurant().getId().toString());
                    ;
                    return response;
                }
                jwkToken = tokenService.getJwkToken(m1.getId());
                response.put("token", jwkToken);
                return response;
            }
        }
        LOG.warn("Wrong login");
        throw new EntityNotFoundException("Wrong login");
    }

    public String update(Long memberId, Member member) {
        Member m = memberRepository.findOne(memberId);
        m.setUsername(member.getUsername());
        String pas = cryptWithMD5(member.getPassword());
        m.setPassword(pas);

        memberRepository.save(m);
        return ("Update successful!");

    }

    public String updateWithRestaurant(Long memberId, Long restaurantId, Member member) {
        Member m = memberRepository.findOne(memberId);
        Restaurant res = restaurantRepository.findOne(restaurantId);
        m.setUsername(member.getUsername());
        String pas = cryptWithMD5(member.getPassword());
        m.setPassword(pas);
        m.setRestaurant(res);
        memberRepository.save(m);
        return ("Update successful!");

    }
}
