package eu.accesa.reservecluj.service.memberService;

import eu.accesa.reservecluj.model.Member;
import eu.accesa.reservecluj.model.Restaurant;

import java.util.List;
import java.util.Map;

public interface MemberService {

    List<Member> getAllMembers();

    Member getByRestaurant(Restaurant restaurant);

    Member getOne(Long id);

    Member save(Member res);

    void delete(Long id);

    Member addMember(Member member);

    Map<String, String> loginMember(Member member);

    String assocMemberRestaurant(String token, Long id);

    String update(Long memberId, Member member);

    String updateWithRestaurant(Long memberId, Long restaurantId, Member member);
}