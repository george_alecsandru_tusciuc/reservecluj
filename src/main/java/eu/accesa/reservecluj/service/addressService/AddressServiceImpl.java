package eu.accesa.reservecluj.service.addressService;

import eu.accesa.reservecluj.model.Address;
import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.repo.AddressRepository;
import eu.accesa.reservecluj.repo.RestaurantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    private static final Logger LOG = LoggerFactory.getLogger(AddressServiceImpl.class);

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Override
    public Address findAddressForRestaurant(Long id) {
        Restaurant ownerRestaurant = checkIfRestaurantExists(id);
        return addressRepository.findOneByOwnerRestaurant(ownerRestaurant);
    }

    /**
     * It changes the adress of a restaurant. In the case that the adress
     * is misssing it adds it in the database
     * @param id        The id of the restaurant
     * @param address   The new address that is going to replace the old one
     * @return          It returns the address of the restaurant
     */
    @Override
    public Address changeAddressForRestaurant(Long id, Address address) {
        Restaurant ownerRestaurant = checkIfRestaurantExists(id);
        Address existingAddress = addressRepository.findOneByOwnerRestaurant(ownerRestaurant);
        if (existingAddress == null) {
            address.setOwnerRestaurant(ownerRestaurant);
            addressRepository.saveAndFlush(address);
            return address;
        } else {
            existingAddress.setStreet(address.getStreet());
            existingAddress.setLatitude(address.getLatitude());
            existingAddress.setLongitude(address.getLongitude());
            addressRepository.saveAndFlush(existingAddress);
            return existingAddress;
        }
    }

    private Restaurant checkIfRestaurantExists(Long id) throws IllegalArgumentException {
        Restaurant ownerRestaurant;
        ownerRestaurant = restaurantRepository.findOne(id);
        if (ownerRestaurant == null) {
            String errorMessage = "Restaurant with id = " + id + " does not exist.";
            LOG.warn(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }
        return ownerRestaurant;
    }
}
