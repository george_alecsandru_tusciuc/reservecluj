package eu.accesa.reservecluj.service.addressService;

import eu.accesa.reservecluj.model.Address;
import org.springframework.stereotype.Service;

@Service
public interface AddressService {
    Address findAddressForRestaurant(Long id);

    Address changeAddressForRestaurant(Long id, Address address);
}
