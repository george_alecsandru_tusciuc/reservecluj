package eu.accesa.reservecluj.repo;

import eu.accesa.reservecluj.model.Reservation;
import eu.accesa.reservecluj.model.ReservationStatus;
import eu.accesa.reservecluj.model.Restaurant;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation,Long> {
    Reservation findById(Long id);
    List<Reservation> findByRestaurant(Restaurant restaurant);
    List<Reservation> findByEmail(String eMail);
    List<Reservation> findFirst10ByEmailOrderByDateDesc(String email);
    List<Reservation> findByDate(LocalDate date);
    List<Reservation> findByHour(int hour);
    List<Reservation> findByNrPersons(int nrPersons);
    List<Reservation> findByReservationStatus(ReservationStatus status);
    Long countByEmailAndRestaurantId(String email, Long restaurantId);
    Reservation findByIdAndRestaurantId(Long Id,Long restaurantId);

}
