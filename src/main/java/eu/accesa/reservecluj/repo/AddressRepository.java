package eu.accesa.reservecluj.repo;

import eu.accesa.reservecluj.model.Address;
import eu.accesa.reservecluj.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {

    Address findOneByOwnerRestaurant(Restaurant ownerRestaurant);

}
