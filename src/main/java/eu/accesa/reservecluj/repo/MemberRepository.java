package eu.accesa.reservecluj.repo;

import eu.accesa.reservecluj.model.Member;
import eu.accesa.reservecluj.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {

    Member findByUsername(String username);

    Member findByRestaurant(Restaurant restaurant);

}

