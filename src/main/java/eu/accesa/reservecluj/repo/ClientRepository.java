package eu.accesa.reservecluj.repo;

import eu.accesa.reservecluj.model.Client;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client,Long> {
    Client findByEmail(String email);
    Client findById(Long id);
}


