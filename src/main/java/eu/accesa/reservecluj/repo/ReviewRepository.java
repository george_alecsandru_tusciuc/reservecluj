package eu.accesa.reservecluj.repo;

import eu.accesa.reservecluj.model.Restaurant;
import eu.accesa.reservecluj.model.Review;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
    List<Review> findByDate(LocalDate date);
    List<Review> findByRestaurantId (Long restaurantId);
    Review findByRestaurantIdAndReservationId(Long restaurantId, Long reservationId);
}
