package eu.accesa.reservecluj.repo;

import eu.accesa.reservecluj.model.FeatureCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface FeatureCategoryRepository extends JpaRepository<FeatureCategory, Long> {

    List<FeatureCategory> findByNameIgnoreCase(String name);

}