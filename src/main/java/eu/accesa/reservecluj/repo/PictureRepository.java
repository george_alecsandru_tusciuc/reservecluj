package eu.accesa.reservecluj.repo;

import eu.accesa.reservecluj.model.Picture;
import eu.accesa.reservecluj.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PictureRepository extends JpaRepository<Picture, Long> {

    List<Picture> findByRestaurant(Restaurant restaurant);

}
