package eu.accesa.reservecluj.repo;

import eu.accesa.reservecluj.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;


public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    Schedule findByRestaurantId(@Param("id") Long restaurantId);
}
