package eu.accesa.reservecluj.converter;

import eu.accesa.reservecluj.controller.dto.FeatureDTO;
import eu.accesa.reservecluj.model.Feature;
import eu.accesa.reservecluj.service.featureService.FeatureCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FeatureConverter {
    @Autowired
    private FeatureCategoryService featureCategoryService;

    public Feature convertToRestaurantProperty(FeatureDTO featureDTO) {
        Feature feature = new Feature();
        feature.setName(featureDTO.getName().toUpperCase());
        feature.setCategory(featureCategoryService.getFeatureCategoryByName(featureDTO.getCategory()));
        return feature;
    }
}
