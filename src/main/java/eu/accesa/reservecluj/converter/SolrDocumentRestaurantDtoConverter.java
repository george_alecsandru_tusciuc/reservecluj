package eu.accesa.reservecluj.converter;

import eu.accesa.reservecluj.controller.dto.RestaurantDto;
import eu.accesa.reservecluj.model.Address;
import eu.accesa.reservecluj.model.Feature;
import eu.accesa.reservecluj.model.Schedule;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;

import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

public class SolrDocumentRestaurantDtoConverter {
    public RestaurantDto convertDocumentToDto(SolrDocument solrDocument) {
        RestaurantDto restaurantDto = new RestaurantDto();
        restaurantDto.setDescription(solrDocument.getFieldValue("description").toString());
        restaurantDto.setFeatures(((List<String>) solrDocument.getFieldValue("features")).stream().map((s) -> {
            Feature p = new Feature();
            p.setName(s);
            return p;
        }).collect(Collectors.toList()));
        restaurantDto.setId(Long.valueOf(solrDocument.getFieldValue("id").toString()));

        Address address = new Address();
        address.setLatitude(Double.valueOf(solrDocument.getFieldValue("latitude").toString()));
        address.setLongitude(Double.valueOf(solrDocument.getFieldValue("longitude").toString()));
        address.setStreet(solrDocument.getFieldValue("street").toString());

        restaurantDto.setAddress(address);

        restaurantDto.setLinkSocial(solrDocument.getFieldValue("linkSocial").toString());

        Schedule schedule = new Schedule();
        schedule.setWeekendDaysClosingHour(LocalTime.parse(solrDocument.getFieldValue("weekend_days_closing_hour").toString()));
        schedule.setWeekendDaysOpeningHour(LocalTime.parse(solrDocument.getFieldValue("weekend_days_opening_hour").toString()));
        schedule.setWorkDaysOpeningHour(LocalTime.parse(solrDocument.getFieldValue("working_days_opening_hour").toString()));
        schedule.setWorkDaysClosingHour(LocalTime.parse(solrDocument.getFieldValue("working_days_closing_hour").toString()));
        restaurantDto.setSchedule(schedule);
        restaurantDto.setName(solrDocument.getFieldValue("name").toString());
        return restaurantDto;
    }

    public SolrInputDocument convertDtoToDocument(RestaurantDto restaurantDto) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        solrInputDocument.addField("description", restaurantDto.getDescription());
        List<String> featureNames = restaurantDto.getFeatures().stream()
                .map(Feature::getName)
                .collect(Collectors.toList());
        solrInputDocument.addField("features", featureNames);
        solrInputDocument.addField("id", restaurantDto.getId().toString());
        solrInputDocument.addField("latitude", restaurantDto.getAddress().getLatitude().toString());
        solrInputDocument.addField("longitude", restaurantDto.getAddress().getLongitude());
        solrInputDocument.addField("linkSocial", restaurantDto.getLinkSocial());
        solrInputDocument.addField("weekend_days_closing_hour", restaurantDto.getSchedule().getWeekendDaysClosingHour().toString());
        solrInputDocument.addField("weekend_days_opening_hour", restaurantDto.getSchedule().getWeekendDaysOpeningHour().toString());
        solrInputDocument.addField("working_days_opening_hour", restaurantDto.getSchedule().getWorkDaysOpeningHour().toString());
        solrInputDocument.addField("working_days_closing_hour", restaurantDto.getSchedule().getWorkDaysClosingHour().toString());
        solrInputDocument.addField("name", restaurantDto.getName());
        solrInputDocument.addField("street", restaurantDto.getAddress().getStreet());
        return solrInputDocument;
    }
}
