package eu.accesa.reservecluj.model;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min=4, message = "Email too short")
    @NotNull(message = "Email cannot be empty")
    @Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*+@[A-Za-z]+[.][A-Za-z]{2,}", message = "Invalid email address")
    private String email;

//    @NotNull(message = "Telephone number cannot be null") @Size(min = 5, max = 10, message = "Invalid telephone number")
//    @Pattern(regexp="^(0[0-9][0-9]*)$", message = "Telephone number can only contain numbers and must start with 0")
    private String telephoneNumber;

    @NotNull(message = "Date cannot be null")
    @DateTimeFormat(pattern="dd-MM-yyyy")
    private LocalDate date;

    @NotNull(message = "The number of persons must be specified")
    @Min(value = 1, message = "Too few persons") @Max(value = 10, message = "Maximum number of persons exceeded")
    private int nrPersons;

    @NotNull(message = "Specify the hour of the reservation")
    private LocalTime hour;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @JsonIgnore
    private Restaurant restaurant;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ReservationStatus reservationStatus = ReservationStatus.PENDING;

    @NotNull(message = "Specify name")
    @Length(min = 2, message = "Name can have at least 2 characters.")
    private String name;

    public Reservation() {
    }

    public Reservation(String email, String telephoneNumber, LocalDate date, int nrPersons, LocalTime hour, Restaurant restaurant, ReservationStatus reservationStatus, String name) {
        this.email = email;
        this.telephoneNumber = telephoneNumber;
        this.date = date;
        this.nrPersons = nrPersons;
        this.hour = hour;
        this.restaurant = restaurant;
        this.reservationStatus = reservationStatus;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Reservation(Long Id, String email, String telephoneNumber, LocalDate date, int nrPersons, LocalTime hour, Restaurant restaurant, ReservationStatus reservationStatus) {
        this.email = email;
        this.telephoneNumber = telephoneNumber;
        this.date = date;
        this.nrPersons = nrPersons;
        this.hour = hour;
        this.restaurant = restaurant;
        this.reservationStatus = reservationStatus;
        this.id=Id;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getNrPersons() {
        return nrPersons;
    }

    public void setNrPersons(int nrPersons) {
        this.nrPersons = nrPersons;
    }

    public LocalTime getHour() {
        return hour;
    }

    public void setHour(LocalTime hour) {
        this.hour = hour;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reservation that = (Reservation) o;

        if (nrPersons != that.nrPersons) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        return telephoneNumber != null ? telephoneNumber.equals(that.telephoneNumber) : that.telephoneNumber == null && (date != null ? date.equals(that.date) : that.date == null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (telephoneNumber != null ? telephoneNumber.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + nrPersons;
        return result;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", date=" + date +
                ", nrPersons=" + nrPersons +
                '}';
    }
}
