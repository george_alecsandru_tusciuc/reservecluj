package eu.accesa.reservecluj.model;

public enum NoiseRating {
    LOW,
    MODERATE,
    HIGH
}
