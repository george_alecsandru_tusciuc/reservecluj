package eu.accesa.reservecluj.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Street parameter is requested.")
    @NotEmpty(message = "Street field is missing.")
    @Length(min = 3, max = 100, message = "Street can have between 3 and 100 characters.")
    private String street;

    @NotNull(message = "Latitude parameter is requested.")
    @DecimalMin(value = "-90", message = "Latitude cannot be less than -90.")
    @DecimalMax(value = "90", message = "Latitude cannot be higher than 90.")
    private Double latitude;

    @NotNull(message = "Longitude parameter is requested.")
    @DecimalMin(value = "-180", message = "Longitude cannot be less than -180.")
    @DecimalMax(value = "180", message = "Longitude cannot be higher than 180.")
    private Double longitude;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(unique = true, nullable = false)
    @JsonIgnore
    private Restaurant ownerRestaurant;

    public Address() {
    }

    public Address(String street, Double latitude, Double longitude, Restaurant ownerRestaurant) {
        this.street = street;
        this.latitude = latitude;
        this.longitude = longitude;
        this.ownerRestaurant = ownerRestaurant;
    }

    public Address(String street, double latitude, double longitude) {
        this.street = street;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Address(String street, double latitude, double longitude, Restaurant ownerRestaurant) {
        this.street = street;
        this.latitude = latitude;
        this.longitude = longitude;
        this.ownerRestaurant = ownerRestaurant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Restaurant getOwnerRestaurant() {
        return ownerRestaurant;
    }

    public void setOwnerRestaurant(Restaurant ownerRestaurant) {
        this.ownerRestaurant = ownerRestaurant;
    }

    @Override
    public String toString() {
        return String.format(
                "Address { id: %d, street: '%s', latitude: %f, longitude: %f }",
                id, street, latitude, longitude);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof Address))
            return false;

        Address other = (Address) obj;

        if (id != null ? !id.equals(other.getId()) : other.getId() != null) return false;
        if (street != null ? !street.equals(other.getStreet()) : other.getStreet() != null)
            return false;
        if (latitude != null ? !latitude.equals(other.getLatitude()) : other.getLatitude() != null)
            return false;
        return longitude != null ? longitude.equals(other.getLongitude()) : other.getLongitude() == null;
    }

    @Override
    public int hashCode() {
        int result = this.getId() != null ? this.getId().hashCode() : 0;
        result = 31 * result + (this.getStreet() != null ? this.getStreet().hashCode() : 0);
        result = 31 * result + (this.getLatitude() != null ? this.getStreet().hashCode() : 0);
        result = 31 * result + (this.getLongitude() != null ? this.getStreet().hashCode() : 0);
        return result;
    }
}
