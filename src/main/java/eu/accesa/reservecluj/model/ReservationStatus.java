package eu.accesa.reservecluj.model;

public enum ReservationStatus {
    PENDING,
    CONFIRMED,
    DENIED,
    DONE,
    NOT_PROCESSED,
    NEW,
    CANCELLED,
    NO_SHOW,
    NO_RESPONSE,
    FINAL
}
