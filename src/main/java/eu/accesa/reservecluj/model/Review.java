package eu.accesa.reservecluj.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
@Entity
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Date cannot be null")
    @DateTimeFormat(pattern = "dd-MMM-yyyy")
    private LocalDate date;

    @NotNull(message = "You must specify the restaurant")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @JsonIgnore
    private Restaurant restaurant;

    @OneToOne
    @JoinColumn(unique=true,nullable = false)
    @NotNull(message = "You must specify the reservation")
    @JsonIgnore
    private Reservation reservation;

    @NotNull(message = "You must provide a message")
    @Column(length = 2000)
    private String message;

    @Min(1)@Max(5)
    private double foodRating;

    @Min(1)@Max(5)
    private double serviceRating;

    @Min(1)@Max(5)
    private double ambienceRating;

    @Min(1)@Max(5)
    private double valueRating;

    @Enumerated(EnumType.STRING)
    private NoiseRating noiseRating;

    public Review() {
    }

    public Review(LocalDate date, Restaurant restaurant, Reservation reservation, String message, double foodRating, double serviceRating, double ambienceRating, double valueRating, NoiseRating noiseRating) {
        this.date = date;
        this.restaurant = restaurant;
        this.reservation = reservation;
        this.message = message;
        this.foodRating = foodRating;
        this.serviceRating = serviceRating;
        this.ambienceRating = ambienceRating;
        this.valueRating = valueRating;
        this.noiseRating = noiseRating;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getFoodRating() {
        return foodRating;
    }

    public void setFoodRating(double foodRating) {
        this.foodRating = foodRating;
    }

    public double getServiceRating() {
        return serviceRating;
    }

    public void setServiceRating(double serviceRating) {
        this.serviceRating = serviceRating;
    }

    public double getAmbienceRating() {
        return ambienceRating;
    }

    public void setAmbienceRating(double ambienceRating) {
        this.ambienceRating = ambienceRating;
    }

    public double getValueRating() {
        return valueRating;
    }

    public void setValueRating(double valueRating) {
        this.valueRating = valueRating;
    }

    public NoiseRating getNoiseRating() {
        return noiseRating;
    }

    public void setNoiseRating(NoiseRating noiseRating) {
        this.noiseRating = noiseRating;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", date=" + date +
                ", message='" + message + '\'' +
                ", foodRating=" + foodRating +
                ", serviceRating=" + serviceRating +
                ", ambienceRating=" + ambienceRating +
                ", valueRating=" + valueRating +
                ", noiseRating=" + noiseRating +
                '}';
    }
}
