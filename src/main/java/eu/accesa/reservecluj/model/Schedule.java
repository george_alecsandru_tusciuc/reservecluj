package eu.accesa.reservecluj.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Entity
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private LocalTime workDaysOpeningHour;
    @NotNull
    private LocalTime workDaysClosingHour;
    @NotNull
    private LocalTime weekendDaysOpeningHour;
    @NotNull
    private LocalTime weekendDaysClosingHour;

    private String utcTimeZone;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, unique = true)
    @JsonIgnore
    private Restaurant restaurant;

    public Schedule() {
        utcTimeZone = "+02:00";
    }

    public Schedule(LocalTime workDaysOpeningHour, LocalTime workDaysClosingHour, LocalTime weekendDaysOpeningHour,
                    LocalTime weekendDaysClosingHour, String utcTimeZone) {
        this.workDaysOpeningHour = workDaysOpeningHour;
        this.workDaysClosingHour = workDaysClosingHour;
        this.weekendDaysOpeningHour = weekendDaysOpeningHour;
        this.weekendDaysClosingHour = weekendDaysClosingHour;
        this.utcTimeZone = utcTimeZone;
    }

    public Schedule(LocalTime workDaysOpeningHour, LocalTime workDaysClosingHour, LocalTime weekendDaysOpeningHour,
                    LocalTime weekendDaysClosingHour) {
        this.workDaysOpeningHour = workDaysOpeningHour;
        this.workDaysClosingHour = workDaysClosingHour;
        this.weekendDaysOpeningHour = weekendDaysOpeningHour;
        this.weekendDaysClosingHour = weekendDaysClosingHour;
        this.utcTimeZone = "+02:00";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalTime getWorkDaysOpeningHour() {
        return workDaysOpeningHour;
    }

    public void setWorkDaysOpeningHour(LocalTime workDaysOpeningHour) {
        this.workDaysOpeningHour = workDaysOpeningHour;
    }

    public LocalTime getWorkDaysClosingHour() {
        return workDaysClosingHour;
    }

    public void setWorkDaysClosingHour(LocalTime workDaysClosingHour) {
        this.workDaysClosingHour = workDaysClosingHour;
    }

    public LocalTime getWeekendDaysOpeningHour() {
        return weekendDaysOpeningHour;
    }

    public void setWeekendDaysOpeningHour(LocalTime weekendDaysOpeningHour) {
        this.weekendDaysOpeningHour = weekendDaysOpeningHour;
    }

    public LocalTime getWeekendDaysClosingHour() {
        return weekendDaysClosingHour;
    }

    public void setWeekendDaysClosingHour(LocalTime weekendDaysClosingHour) {
        this.weekendDaysClosingHour = weekendDaysClosingHour;
    }

    public String getUtcTimeZone() {
        return utcTimeZone;
    }

    public void setUtcTimeZone(String utcTimeZone) {
        this.utcTimeZone = utcTimeZone;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Schedule)) return false;

        Schedule schedule = (Schedule) o;

        if (getId() != null ? !getId().equals(schedule.getId()) : schedule.getId() != null)
            return false;
        if (getWorkDaysOpeningHour() != null ? !getWorkDaysOpeningHour().equals(schedule.getWorkDaysOpeningHour()) : schedule.getWorkDaysOpeningHour() != null)
            return false;
        if (getWorkDaysClosingHour() != null ? !getWorkDaysClosingHour().equals(schedule.getWorkDaysClosingHour()) : schedule.getWorkDaysClosingHour() != null)
            return false;
        if (getWeekendDaysOpeningHour() != null ? !getWeekendDaysOpeningHour().equals(schedule.getWeekendDaysOpeningHour()) : schedule.getWeekendDaysOpeningHour() != null)
            return false;
        return getWeekendDaysClosingHour() != null ? getWeekendDaysClosingHour().equals(schedule.getWeekendDaysClosingHour()) : schedule.getWeekendDaysClosingHour() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getWorkDaysOpeningHour() != null ? getWorkDaysOpeningHour().hashCode() : 0);
        result = 31 * result + (getWorkDaysClosingHour() != null ? getWorkDaysClosingHour().hashCode() : 0);
        result = 31 * result + (getWeekendDaysOpeningHour() != null ? getWeekendDaysOpeningHour().hashCode() : 0);
        result = 31 * result + (getWeekendDaysClosingHour() != null ? getWeekendDaysClosingHour().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", workDaysOpeningHour=" + workDaysOpeningHour +
                ", workDaysClosingHour=" + workDaysClosingHour +
                ", weekendDaysOpeningHour=" + weekendDaysOpeningHour +
                ", weekendDaysClosingHour=" + weekendDaysClosingHour +
                '}';
    }
}
