package eu.accesa.reservecluj;

import eu.accesa.reservecluj.service.security.RestaurantAccessFilter;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import javax.servlet.Filter;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "eu.accesa")
public class AppConfig {

    @Bean
    public FilterRegistrationBean restaurantAccessFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(someFilter());
        registration.addUrlPatterns("/aop/*");
        registration.setName("someFilter");
        registration.setOrder(1);
        return registration;
    }

    @Bean(name = "memberAccessFilter")
    public Filter someFilter() {
        return new RestaurantAccessFilter();
    }
}

