(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['subscription'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"col-1-2 subscription-container\">\r\n    <div class=\"subscription-form-wrapper\">\r\n        <h3 class=\"subscription-headline\"> Most popular restaurant</h3><br>\r\n        <p> Get latest offers from the most popular restaurant</p><br/>\r\n        <form id=\"subscriptionForm\" class=\"clearfix\" action=\"#notify\">\r\n            <input class=\"newsletter-email\" type=\"text\" name=\"email\" placeholder=\"Email Address\">\r\n            <input class=\"newsletter-submit\" type=\"submit\" value=\"Notify\">\r\n        </form>\r\n    </div>\r\n</div>\r\n<div class=\"col-1-2 most-popular-restaurant\">\r\n    <img src=\"images/header_image.jpg\">\r\n</div>\r\n\r\n";
},"useData":true});
})();