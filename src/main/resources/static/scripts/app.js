(function () {
    'use strict';
    if (!window.jQuery && !window.Handlebars && !window.Path) {
        console.error('Dependency(ies) not available, exiting!');
        return;
    }

    //extend existing APP global namespace or create an empty APP global namespace
    window['APP'] = $.extend(window['APP'] || {}, {});
    var $mainContainer = $('#mainContainer'),
        $featuredContainer = $('#featured'),
        $filterSearchContainer = $('#filterSearchSection'),
        $restaurantsContainer = $('#restaurants'),
        $subscriptionContainer = $('#subscription'),
        $filterContainer = $('#filter'),
        $socialContainer = $('#social'),
        $reservationBar = $('#reservationBar'),
        markersArray = [],
        changedImages = [],
        $filterHomePage = $('#filterHomePage'),
        $filterAndRestaurants = $('#filter-restaurants'),
        $profileModal = $('#my-profile-modal'),
        $currentRestaurantId,
        errorFieldsMap = {},
        /**
         * Binds event handlers to HTML elements that are (or will be later rendered) in the page
         */
        bindEventHandlers = function () {
            $(document)
                .on('click', '#profileBtn', function(e){
                    e.preventDefault();

                    renderProfileModal();
                })
                .on('click', '#profile-close', function (e) {
                    e.preventDefault();
                    $profileModal.empty();
                })
                .on('click','#homePageButton',function () {
                    $('.search-container').show();
                    window.location.href='#index';
                })
                .on('click','#reviewPageSubmit',function (e) {
                    e.preventDefault();
                    var currentDate=new Date();
                    var date= [currentDate.getFullYear(),currentDate.getMonth()+1,currentDate.getDate()];
                    var restaurantId=$('#restaurantId').val();
                    var reservationId=$('#reservationId').val();
                    var message=$('#reviewMessage').val();
                    var selected = $("input[type='radio'][name='foodRating']:checked");
                    var foodRating;
                    if (selected.length > 0) {
                       foodRating=selected.val();
                    }
                    if(foodRating==undefined){
                        $('#information').html("<p>Please complete all the fields</p>");
                        return false;
                    }
                    selected = $("input[type='radio'][name='ambienceRating']:checked");
                    var ambienceRating;
                    if (selected.length > 0) {
                        ambienceRating=selected.val();
                    }
                    if(ambienceRating==undefined){
                        $('#information').html("<p>Please complete all the fields</p>");
                        return false;
                    }
                    selected = $("input[type='radio'][name='serviceRating']:checked");
                    var serviceRating;
                    if (selected.length > 0) {
                        serviceRating=selected.val();
                    }
                    if(serviceRating==undefined){
                        $('#information').html("<p>Please complete all the fields</p>");
                        return false;
                    }
                    selected = $("input[type='radio'][name='valueRating']:checked");
                    var valueRating;
                    if (selected.length > 0) {
                        valueRating=selected.val();
                    }
                    if(valueRating==undefined){
                        $('#information').html("<p>Please complete all the fields</p>");
                        return false;
                    }
                    var noiseRating=$('#noiseLevel').val();
                    var json={'date':date,'ambienceRating':ambienceRating,
                        'foodRating':foodRating,
                        'noiseRating':noiseRating,
                        'serviceRating':serviceRating,
                        'valueRating':valueRating,
                        'message':message};
                    saveReview(json,restaurantId,reservationId);
                })
                .on('click', '#profileBtn', function(e){
                    e.preventDefault();

                    renderProfileModal();
                })
                .on('click', '#profile-close', function (e) {
                    e.preventDefault();
                    $profileModal.html("");
                })
                .on('submit', '#subscriptionForm', function (e) {
                    e.preventDefault();
                })
                .on('submit', '.search-form, .header-search-form', function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    if ($this.is('.header-search-form')) {
                        var value = $(this).find('input[type="search"]').val();
                        $('.filter-criteria:checked').removeAttr('checked');
                        $.getJSON('restaurant/search', {query: value}, function (response) {
                            renderRestaurants(response);
                        });
                    }
                })
                .on('click', '.filter-option', function () {
                    $('html,body').scrollTop(0);
                    $('body').toggleClass('disable-scroll');
                    $('#filter').removeClass('hidden');
                })
                .on('focus', function () {
                    $('.login').addClass('focused');
                })
                .on('click', '.reset-filter-button', function (e) {
                    var filters = $('.filter-criteria'), i;
                    for (i = 0; i < filters.length; ++i) {
                        filters[i].checked = false;
                    }
                })
                .on('change', '.filter-criteria', function () {
                    var categories = Object.keys(window.APP.filters);
                    var json = {};
                    for (var i = 0; i < categories.length; ++i) {
                        var checkBoxes = $(this).closest('.filter-home-page').find('.filter-criteria:checked');
                        var array = [];
                        for (var j = 0; j < checkBoxes.length; j++) {
                            array.push($(checkBoxes[j]).val());
                        }
                        json[categories[i].toLowerCase()] = array;
                    }
                    filterRestaurants(json);
                    return false;
                })
                .on('keyup', '.search-input', function (e) {
                    var searchTerm = $(this).val();
                    if (e.which === 13) {
                        e.preventDefault();
                    }
                    if (APP.restaurants.length) {
                        var filteredRestaurants = APP.restaurants.filter(function (item) {
                            //match the search term against a RegExp (options: case-insensitive,
                            // greedy)
                            return new RegExp(searchTerm, 'ig').test(item.name);
                        });
                        renderRestaurants(filteredRestaurants);
                    }
                })
                .on('search', '.search-input', function (e) {
                    if ($(this).val().length) {
                        return;
                    }
                    renderRestaurants(APP.restaurants);
                })
                .on('click', '#filter-close', function (e) {
                    $('body').removeClass('disable-scroll');
                    $('#filter').toggleClass('hidden');
                    return false;
                })
                .on('click', '.bt-menu-trigger, .close-nav', function (e) {
                    var $mainNav = $('#mainNav');
                    $(this).is('.bt-menu-trigger') ? $mainNav.addClass('slideDown').removeClass('slideUp')
                        : $mainNav.removeClass('slideDown').addClass('slideUp');
                    return false;
                })
                .on('submit', '.login-form', function (e) {
                    e.preventDefault();
                    doLogIn();

                })
                .on('mouseenter mouseleave', '.bt-menu-trigger', function (e) {
                    $(this).toggleClass('bt-menu-open');
                })
                .on('click', '#homePageMenuItem', function (event) {
                    window.location.href = 'http://localhost:8080/reservecluj';
                })
                .on('click', '#navMenuForDetails ul li a', function (event) {
                    event.preventDefault();
                    var nextSection = $(this).closest('section').next().attr("id");
                    $.mPageScroll2id("scrollTo", nextSection);

                })
                .on('submit', '#update-form', function (e) {
                    if(event.keyCode == 13) {
                        event.preventDefault();
                        console.log('form tried to submit with enter');
                        return false;
                    }
                    e.preventDefault();
                    $.ajax({
                               contentType:'application/json',
                               method:'PUT',
                               url:'restaurant/' + $currentRestaurantId,
                               data:updateData()
                           }).done(function (response) {
                    });
                    // send only changed images
                    var imgUrl = "";
                    $.each(changedImages,function (index, img) {
                        if (img.id < 1) {
                            imgUrl = 'restaurant/' + $currentRestaurantId + '/pictures/url';
                        }
                        else {
                            imgUrl = 'restaurant/' + $currentRestaurantId + '/pictures/' + img.id;
                        }
                        $.ajax({

                                   contentType:'application/json; charset:UTF-8',
                                   method:'POST',
                                   url: imgUrl,
                                   data:{'path' : img.path}
                               }).done(function (response) {
                        });
                    });
                    window.scrollTo(0, 0);
                })
                .on('click', '.accept, .decline', function (e) {
                    var $eventTarget = $(e.target);
                    ticketUpdate($eventTarget.parent().parent().data("id"),$eventTarget.val(),$eventTarget.parent().parent().children('textarea').val());

                })
                .on('click', '.done, .noShow', function (e) {
                    var $eventTarget = $(e.target);
                    ticketUpdate($eventTarget.parent().parent().data("id"),$eventTarget.val());

                })
                .on('click', '.select', function (e) {
                    showDivs($(e.target));
                })
                .on('click','#btnLogOut',function(e){

                    logout();
                })
                .on('click', '.delete-img-button', function (e) {
                    $.ajax({
                               contentType:'application/json',
                               method:'DELETE',
                               url:'restaurant/' + $currentRestaurantId + '/pictures/' + $(e.target).data('id')
                           }).done(function (response) {
                        console.log(response);
                    });
                    $(e.target).parent().remove();
                })
                .on('click', '#add-image-btn',function (e) {
                    var tpl = Handlebars.templates['imageSelector'];
                    $('#add-image-btn').before(tpl({
                                            "id": 0,
                                            "path": "http://www.vishmax.com/en/innovattive-cms/themes/themax-theme-2015/images/no-image-found.gif"
                                           }))
                })
                .on('keydown', '#img-path', function (e) {
                    if(event.keyCode == 13) {
                        event.preventDefault();
                        var setPath = $('#img-path').val();
                        if (setPath.localeCompare($('#img-holder')[0].src) != 0){
                            var selectedImg = $("img[data-id='" + window.APP.selectedImg.data('id') + "']")[0];
                            selectedImg.src = setPath;
                            var wasChanged = false;
                            $.each(changedImages, function (ind, image) {
                                if (image.id === window.APP.selectedImg.data('id')){
                                    wasChanged = true;
                                    image.path = setPath;
                                }
                            });
                            if (!wasChanged) {
                                changedImages.push({
                                                       path: setPath,
                                                       id: window.APP.selectedImg.data('id')
                                                   });
                            }
                            selectedImg.click();
                        }
                        return false;
                    }
                })
                .on('click', '[data-popup-close]', function(e)  {
                    var targeted_popup_class = $(this).attr('data-popup-close');
                    $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                    e.preventDefault();
                })
                .on('click', '#openDetailsMenu', function(event)  {
                    event.preventDefault();
                    $('#detailsNavOption').toggle('fast', function() {
                        $('#detailsNavOption').toggleClass('invisible');
                        $('#openDetailsMenu').toggleClass('fa-arrow-right').toggleClass('fa-arrow-left');
                    });
                })
                .on('click', '#mostVisited', function (e) {
                    $.getJSON('restaurant/visited', function (response) {
                        renderRestaurants(response);
                    });
                })
                .on('click', '#mostPopular', function (e) {
                    $.getJSON('restaurant/popular', function (response) {
                        renderRestaurants(response);
                    });
                })
                .on('click', '#smartestChoice', function (e) {
                    $.getJSON('restaurant/popular', function (response) {
                        renderRestaurants(response);
                    });
                })
                .on('click', '#reserveButton ', function (e) {
                    var data = {};
                    data['date'] = $('#reserveDateInput').val().split('-').reverse();
                    if(data['date'].length < 3)
                        data['date'] = null;
                    else
                        for (var i=0;i<data['date'].length;++i) data['date'][i]=parseInt(data['date'][i]);
                    data['hour']=$('#reserveTimeInput').val().split(':');
                    if(data['hour'].length < 2)
                        data['hour'] = null;
                    else
                        for (i=0;i<data['hour'].length;++i) data['hour'][i]=parseInt(data['hour'][i]);
                    data['email'] = $('#reserveEmailInput').val();
                    data['telephoneNumber'] = $('#reservePhoneInput').val();
                    data['name'] = $('#reserveNameInput').val();
                    data['nrPersons'] = parseInt($('#reservePersonInput').val());
                    $.ajax({
                            contentType: 'application/json',
                            method: 'post',
                            url: 'reservation'+'?restaurantId='+$currentRestaurantId,
                            data: JSON.stringify(data),
                            success: function(){
                                renderPopupForConfirmation('Your reservation has been submitted!');
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                var map = {
                                            'email' : 'reserveEmailInputError' ,
                                            'telephoneNumber' : 'reservePhoneInputError' ,
                                            'name' : 'reserveNameInputError',
                                            'hour' : 'reserveTimeInputError',
                                            'date' : 'reserveDateInputError'
                                            };
                                var responseJSON = XMLHttpRequest.responseJSON;
                                for (var key in responseJSON){
                                    if (responseJSON.hasOwnProperty(key)){
                                        if ($('#' + map[key]).hasClass('hidden')) {
                                            $('#' + map[key]).toggleClass('hidden');
                                        }
                                        $('#' + map[key]).html(responseJSON[key].replace('\n', '<br>'));
                                    }
                                }
                            }
                        }
                    )
                })
                .on('blur', '#reserveNameInput', function (event) {
                    var textFromInput = $('#reserveNameInput').val();
                    var errorContainer = $('#reserveNameInputError');
                    if(textFromInput.length < 2){
                        errorContainer.toggleClass('hidden');
                        errorContainer.text("Name must contain at least 2 characters");
                    }
                })
                .on('focus', '#reserveNameInput', function (event) {
                    var container = $('#reserveNameInputError');
                    if (!container.hasClass('hidden')) {
                        container.toggleClass('hidden');
                    }
                })
                .on('blur', '#reserveEmailInput', function (event) {
                    var textFromInput = $('#reserveEmailInput').val();
                    var errorContainer = $('#reserveEmailInputError');
                    var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if(! filter.test(textFromInput)){
                        errorContainer.toggleClass('hidden');
                        errorContainer.text("Invalid email address");
                    }
                })
                .on('focus', '#reserveEmailInput', function (event) {
                    var container = $('#reserveEmailInputError');
                    if (!container.hasClass('hidden')) {
                        container.toggleClass('hidden');
                    }
                })
                .on('blur', '#reservePhoneInput', function (event) {
                    var errorDetected = false;
                    var textFromInput = $('#reservePhoneInput').val();
                    var errorContainer = $('#reservePhoneInputError');
                    var filter = /^(0[0-9][0-9]*)$/;
                    if(! filter.test(textFromInput)){
                        errorDetected = true;
                        errorContainer.text("Telephone number can only contain numbers and must start with 0");
                    }
                    if( textFromInput.length < 5 || textFromInput.length > 10 ){
                        errorDetected = true;
                        errorContainer.text("Telephone number can have bettween 5-10 digits");
                    }
                    if( errorDetected ) {
                        errorContainer.toggleClass('hidden');
                    }
                })
                .on('focus', '#reservePhoneInput', function (event) {
                    var container = $('#reservePhoneInputError');
                    if (!container.hasClass('hidden')) {
                        container.toggleClass('hidden');
                    }
                })
                .on('click', 'a.fa-rocket', function (e) {
                    e.preventDefault();
                    var data = {};
                    data['date'] = $('#reserveDateInput').val().split('-').reverse();
                    if(data['date'].length < 3)
                        data['date'] = null;
                    else
                        for (var i=0;i<data['date'].length;++i) data['date'][i]=parseInt(data['date'][i]);
                    data['hour']=$('#reserveTimeInput').val().split(':');
                    if(data['hour'].length < 2)
                        data['hour'] = null;
                    else
                        for (i=0;i<data['hour'].length;++i) data['hour'][i]=parseInt(data['hour'][i]);
                    data['email'] = window.APP.clientEmail;
                    data['telephoneNumber'] = $('#reservePhoneInput').val();
                    data['name'] = window.APP.clientName;
                    data['nrPersons'] = parseInt($('#reservePersonInput').val());
                    $.ajax({
                        contentType: 'application/json',
                        method: 'post',
                        url: 'reservation' + '?restaurantId=' + $(e.target).data('restaurant-id'),
                        data: JSON.stringify(data),
                        success: function () {
                            renderPopupForConfirmation('Your reservation has been submitted!');
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var map = {
                                'email': 'reserveEmailInputError',
                                'telephoneNumber': 'reservePhoneInputError',
                                'hour': 'reserveTimeInputError',
                                'date': 'reserveDateInputError'
                            };
                            var responseJSON = XMLHttpRequest.responseJSON;
                            for (var key in responseJSON) {
                                if (responseJSON.hasOwnProperty(key)) {
                                    if ($('#' + map[key]).hasClass('hidden')) {
                                        $('#' + map[key]).toggleClass('hidden');
                                    }
                                    $('#' + map[key]).html(responseJSON[key].replace('\n', '<br>'));
                                }
                            }
                            $( function() {
                                var paragraph = $('<p></p>');
                                if (window.APP.clientName == '')
                                    paragraph.text('Please login first');
                                else
                                    paragraph.text('Your reservation has not been submitted');
                                $("#reservationConfirmationContainer").html(paragraph);
                                $("#popupConfirmation").fadeIn(350);
                            });
                        }
                    })
                })
                .on('keydown', '#street', function (e) {
                    if(event.keyCode == 13) {
                        e.preventDefault();
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode( { 'address': $(e.target).val()}, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK)
                            {
                                placeMarker(results[0].geometry.location);
                                window.APP.mainMarker.setPosition( results[0].geometry.location );
                                if( window.APP.map !== undefined)
                                    window.APP.map.panTo( results[0].geometry.location );
                                $("input[name='latitude']").val(results[0].geometry.location.lat());
                                $("input[name='longitude']").val(results[0].geometry.location.lng());
                            }
                        });
                        return false;
                    }
                })
                .on('click', '.reservation-input', function(e){
                    $(this).find('input').focus();
                });
        },
        renderPopupForConfirmation = function(titleText){
            var title = $('<p class="popup-title">' + titleText + '</p>');
            if (window.APP.clientName != ''){
                var name = $('<p class="popup-category-text"><i class="popup-category">Name:</i>' + window.APP.clientName + ' </p>');
                var email = $('<p class="popup-category-text"><i class="popup-category">Email:</i>' + window.APP.clientEmail + '</p>');
            }
            else {
                var name = $('<p class="popup-category-text"><i class="popup-category">Name:</i>' + $('#reserveEmailInput').val() + ' </p>');
                var email = $('<p class="popup-category-text"><i class="popup-category">Email:</i>' + $('#reserveNameInput').val() + '</p>');
            }
            var date = $('<p class="popup-category-text"><i class="popup-category">Date:</i>' + $('#reserveDateInput').val() + '</p>');
            var time = $('<p class="popup-category-text"><i class="popup-category">Time:</i>' + $('#reserveTimeInput').val() + '</p>');
            var nrPersons = $('<p class="popup-category-text"><i class="popup-category">Number of persons:</i>' + '     ' +  $('#reservePersonInput').val() + '</p>');
            $("#reservationConfirmationContainer").empty();
            $("#reservationConfirmationContainer").append(title)
                .append(name)
                .append(email)
                .append(date)
                .append(time)
                .append(nrPersons);
            $("#popupConfirmation").fadeIn(350);
        },
        //updater
        updateData = function (){
            var requestObj = {};
            $("#description-form input,#description-form textarea").each(function (i, elem) {
                requestObj[elem.name] = elem.value;
            });
            requestObj['schedule'] = {};
            $("#schedule-form input").each(function (index, elem) {
                requestObj['schedule'][elem.name] = elem.value.split(":").map(function(item) {
                    return parseInt(item,10);
                });
            });
            requestObj['features'] = [];
            $("#feature-form input").each(function (index, elem) {
                if (elem.checked === true) {
                    requestObj['features'].push({name : elem.value}) ;
                }
            });
            requestObj['address'] = {};
            $("#address-form input").each(function (index, elem) {
                //dumb hardcode gotta push fast
                if (elem.name.localeCompare('street') !== 0)
                    requestObj['address'][elem.name] = parseFloat(elem.value);
                else
                    requestObj['address'][elem.name] = elem.value;
            });
            requestObj['id'] = parseInt($currentRestaurantId);
            console.log(JSON.stringify(requestObj));
            return JSON.stringify(requestObj);
        },
        ticketUpdate = function (id, status, message) {
            if (message === undefined){
                $.ajax({
                           contentType:'application/json',
                           method:'PUT',
                           url:'reservation/' + id + '/status',
                           data:status
                       }).done(function (response) {
                        getTickets();
                        window.scrollTo(0, 0);
                    });

            }else {
                $.ajax({
                           contentType: 'application/json',
                           method: 'PUT',
                           url: 'reservation/' + id + '/status',
                           data: status
                       }).done(function (response) {
                    $.ajax({
                               contentType: 'application/json',
                               method: 'POST',
                               url: 'reservation/' + id + '/confirmation',
                               data: message
                           }).done(function (response) {
                        console.log(response);
                    });
                    getTickets();
                    window.scrollTo(0, 0);
                });
            }
        },
        doLogIn = function () {
            $('.login').removeClass('focused').addClass('loading');
            login();
        },
        saveReview=function (data,restaurantId,reservationId) {
            $.ajax({
                       contentType:'application/json',
                       method:'POST',
                       url:'restaurant/'+restaurantId+'/reservation/'+reservationId+'/review',
                       data:JSON.stringify(data)
                   }).done(function () {
                $('#reviewPageContainer').html("<p>Your review has been submitted</p>")
            }).fail(function () {
                $('#reviewPageContainer').html("<p>Your review has not been submitted</p>")

            })
        },
        filterRestaurants = function (data) {
            $.ajax({
                contentType: 'application/json',
                method: 'POST',
                url: 'restaurant/search',
                data: JSON.stringify(data)
            }).done(function (response) {
                renderRestaurants(response);
            });
        },
        login = function () {
            var username = document.getElementById('username').value;
            var password = document.getElementById('password').value;
            var json = {'username': username, 'password': password};
            $.ajax({
                contentType: 'application/json',
                method: 'POST',
                url: 'member/token',
                data: JSON.stringify(json),
                success: function (response) {
                    window.APP.token = response.token;
                    var link = "#restaurant/" + response.restaurant_id + "/admin";
                    $('#head, #footer').removeClass('hidden');

                    window.location.href = link;
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('wrong username or password');
                    $('#username').val("");
                    $('#password').val("");
                    $('.login').removeClass('loading');
                }
            });
        },
        /**
         * Renders the list of restaurants fetched via AJAX
         *
         * @param data - JSON object that contains the restaurants list
         */
        getCurrentDate = function(){
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd
            }
            if(mm<10){
                mm='0'+mm
            }
            return dd+'-'+mm+'-'+yyyy;
        },
        getCurrentTime = function () {
            function checkTime(i) {
                return (i < 10) ? "0" + i : i;
            }
            var today = new Date(),
                h = checkTime(today.getHours()),
                m = checkTime(today.getMinutes());
            return h + ':' + m;
        },
        renderRestaurants = function (data) {
            for (var i = 0; i<data.length; ++i)
                data[i]['features']=data[i]['features'].slice(0,data[i]['features'].length > 2 ? 2 : data[i]['features'].length);
            var tpl = Handlebars.templates['restaurants'];
            $restaurantsContainer.html(tpl(data));
            getImagesForRestaurants(data);
        },
        renderSubscription = function () {
            var tpl = Handlebars.templates['subscription'];
            $subscriptionContainer.html(tpl);
        },
        renderFilters = function () {
            $filterContainer.html(Handlebars.templates['filters'](window.APP.filters));
        },
        renderProfileModal = function() {
            $.getJSON("reservation/mail",{"reservationEmail":window.APP.clientEmail}
            ).done(function (response) {
                console.log(response);
                for(var i = 0; i<response.length; i++) {
                        var date = response[i].date;
                        var time = new Date;
                        console.log(response[i].hour);
                        time.setHours(response[i].hour[0], response[i].hour[1]);
                        var formatDate = date[2] + "-" + date[1] + "-" + date[0];
                        response[i].hour = time.toTimeString().substr(0, 5);
                        response[i].date = formatDate;
                        response[i].reservationStatus = response[i].reservationStatus.replace("_", " ");

                }
            $.getJSON("restaurant/favorites",{"userEmail":window.APP.clientEmail}
            ).done(function (restaurantResponse){
                var tpl = Handlebars.templates['clientModal']({"restaurants": restaurantResponse,
                                                                  "reservation":response, "picture":window.APP.clientPicture,
                                                                  "username":window.APP.clientName});
                $profileModal.html(tpl);
                getImagesForProfileRestaurants(restaurantResponse);
            })
            })
        },
        renderRestaurantDetailsPage = function (currentRestaurant) {
            $('#head, #footer').toggleClass('hidden');
            $('html,body').scrollTop(0);
            var tpl = Handlebars.templates['restaurantDetailsPage'];
            $mainContainer.html(tpl(currentRestaurant));
            //load action for nav bar
            $("#navMenuForDetails ul li a").mPageScroll2id();
            //render features
            renderFeaturesInDetailsPage(currentRestaurant.features);
            //render schedule
            renderScheduleInDetailsPage(currentRestaurant.id);
            //load image carousel
            getImagesForCarousel(currentRestaurant.id);
            //load reservation modal
            renderReservationModal();
            //render map
            $('#mapContainer').append(Handlebars.templates['googleMap']);
            renderCurrentLocation(currentRestaurant.id);
            $.getJSON('restaurant/'+currentRestaurant.id+'/review/all', function (response) {
                if(window['RTG']){
                    window['RTG'].renderRatings(response, $('#ratingSection'));
                }
            });
            $.getJSON('restaurant/'+currentRestaurant.id+'/review', function (response) {
                if(window['RTG']){
                    window['RTG'].renderComments(response, $('#reviewComments'));
                }
            });
        },
        renderScheduleInDetailsPage = function (idForRestaurant) {
            $.getJSON('restaurant/' + idForRestaurant +'/schedule', function (response) {
                //add schedule to details
                var weekTitle = $('<p />');
                weekTitle.addClass('details-paragraphs-column');
                weekTitle.text("Week days");
                var weekSchedule = $('<p />');
                weekSchedule.addClass('details-paragraphs-column');
                var opening = new Date();
                opening.setHours(response.workDaysOpeningHour[0], response.workDaysOpeningHour[1]);
                var closing = new Date();
                closing.setHours(response.workDaysClosingHour[0], response.workDaysClosingHour[1]);
                weekSchedule.text(opening.toTimeString().substr(0, 5) + " - " + closing.toTimeString().substr(0, 5));
                var weekendTitle = $('<p />');
                weekendTitle.addClass('details-paragraphs-column');
                weekendTitle.text("Weekend days");
                var weekendSchedule = $('<p />');
                weekendSchedule.addClass('details-paragraphs-column');
                opening.setHours(response.weekendDaysOpeningHour[0], response.weekendDaysOpeningHour[1]);
                closing.setHours(response.weekendDaysClosingHour[0], response.weekendDaysClosingHour[1]);
                weekendSchedule.text(opening.toTimeString().substr(0, 5) + " - " + closing.toTimeString().substr(0, 5));
                $('#currentSchedule').append(weekTitle)
                    .append(weekSchedule)
                    .append($('<br>'))
                    .append(weekendTitle)
                    .append(weekendSchedule);
            });
        },
        renderFeaturesInDetailsPage = function(features){
            for(var i = 0; i < features.length; i++){
                var feature = features[i];
                var content = $('<p />');
                content.addClass('details-paragraphs-column');
                content.text(feature.name);
                if(feature.category.name == 'CUISINE'){
                    $('#currentCuisine').append(content);
                } else {
                    $('#currentGeneral').append(content);
                }
            }
        },
        renderCurrentLocation = function (idForRestaurant) {
            $.getJSON('restaurant/' + idForRestaurant +'/address', function (response) {
                //add street address to details
                var street = $('<p />');
                street.addClass('details-paragraphs-column');
                street.text(response.street);
                $('#currentAddress').append(street);

                var coordinates = new google.maps.LatLng(response.latitude, response.longitude);
                var staticMap = initializeStaticGoogleMapInContainer(coordinates, 'mapDetails');
                createFixedMarkerForMap(staticMap, coordinates, createPinWithColor('#48e29a'));
                var dynamicMap = initializeDynamicGoogleMapInContainer(coordinates, 'mapDetailsPopup');
                createFixedMarkerForMap(dynamicMap, coordinates, createPinWithColor('#48e29a'));
                google.maps.event.addListener(staticMap, "click", function (event) {
                    $("#popupMap").fadeIn(350);
                    google.maps.event.trigger(dynamicMap, "resize");
                    dynamicMap.panTo(coordinates);
                })
            });
        },
        renderFiltersHomePage = function () {
            $.getJSON('filters', function (response) {
                var tpl = Handlebars.templates['filtersHomePage'];
                window.APP = $.extend(window.APP, {filters: response});
                $filterHomePage.html(tpl(response));
            })
        },
        renderReservationModal = function () {
            var tpl = Handlebars.templates['reservationModal'];
            $('#reservationSection').append(tpl);
            //add java script for calendar from #reservation section
            $('#dtBox').DateTimePicker();
            //set predefined date and time
            var currentDate = new Date();
            var date= [currentDate.getFullYear(),currentDate.getMonth()+1,currentDate.getDate()];
            $('#reserveDateInput').val(date[2] + "-" + date[1] + "-" + date[0]);
            $('#reserveTimeInput').val(currentDate.toTimeString().substr(0, 5));
            $('#reserveNameInput').val(window.APP.clientName);
            $('#reserveEmailInput').val(window.APP.clientEmail);
        },
        renderReservationBar = function () {
            var tpl = Handlebars.templates['reservationBar'];

            $reservationBar.html(tpl);
            //add java script for calendar from #reservationBar section
            $('#dtBoxBar').DateTimePicker();
        },
        renderHomePage = function () {
            prepareFacebook();
            $mainContainer.empty();
            $mainContainer.append($profileModal)
                .append($featuredContainer)
                .append($filterSearchContainer)
                .append($reservationBar)
                .append($filterAndRestaurants)
                .append($subscriptionContainer)
                .append($socialContainer);

            $.getJSON('restaurant', function (response) {
                //cache the restaurants list for later filtering
                window.APP = $.extend(window.APP, {restaurants: response});
                renderRestaurants(response);
            });
            $('body').removeClass('body-color');
            $('#head, #footer').removeClass('hidden');
            renderSubscription();
            getFilters();
            renderReservationBar();
            renderFiltersHomePage();
            $('#dtBoxBar').DateTimePicker();
            $('[data-field=date]')[0].value = getCurrentDate();
            $('[data-field="time"]')[0].value = getCurrentTime();
            var num = Math.floor( Math.random() * 9 );
            $("#headerLogo").attr("src", "images/img"+num+".jpg");
        },
        getFilters = function () {
            $.getJSON('filters', function (response) {
                var tpl = Handlebars.templates['filters'];
                window.APP = $.extend(window.APP, {filters: response})
            }).done(function (response) {
                renderFilters()
            });
        },
        renderReview=function(restaurantId,reservationId){
            $('.search-container').hide();
            $.getJSON('restaurant/' + restaurantId + '/reservation/'+reservationId+'/review')
                .done(function(response){
                        renderReviewExists();
                }).fail(function () {
                    renderReviewPage(restaurantId,reservationId);
            });
            },
            renderReviewExists=function(){
                $mainContainer.html(Handlebars.templates['reviewExists']);
            },
        renderReviewPage=function(restaurantId,reservationId){
            $.getJSON('restaurant/'+restaurantId, {
            }).done(function (response) {
                $mainContainer.html(Handlebars.templates['reviewPage']({"name":response['name'],"restaurantId":restaurantId,"reservationId":reservationId}));
            });
        },
        renderLogin = function () {
            $('#head, #footer').toggleClass('hidden');
            if (!$('#loginSection').hasClass('hidden')) {
                $('#loginSection').toggleClass('hidden');
            }
            $mainContainer.empty();
            var tpl = Handlebars.templates['authenticate'];
            $('body').toggleClass('body-color');
            $mainContainer.html(tpl);
        },
        renderSearchResults = function (data) {
            $mainContainer.html(Handlebars.templates['restaurants'](data));
        },
        renderImagesForRestaurant = function (images) {
            $('#imageCarousel').append(Handlebars.templates['imageCarousel'](images));
            $("#restaurantImagesCarousel").owlCarousel({
                // Most important owl features
                items : 5,
                itemsCustom : false,
                itemsDesktop : [1199,4],
                itemsDesktopSmall : [980,3],
                itemsTablet: [768,2],
                itemsTabletSmall: false,
                itemsMobile : [479,1],
                singleItem : true,
                itemsScaleUp : 4,
                //Basic Speeds
                slideSpeed : 500,
                paginationSpeed : 800,
                rewindSpeed : 1000,
                //Autoplay
                autoPlay : true,
                stopOnHover : false,
                // Navigation
                navigation : true,
                navigationText : [
                    "<i class='icon-chevron-left icon-white'><</i>",
                    "<i class='icon-chevron-right icon-white'>></i>"
                ],
                rewindNav : true,
                scrollPerPage : false,
                //Pagination
                pagination : true,
                paginationNumbers: false,
                // Responsive
                responsive: true,
                responsiveRefreshRate : 200,
                responsiveBaseWidth: window,
                // CSS Styles
                baseClass : "owl-carousel",
                theme : "owl-theme",
                //Lazy load
                lazyLoad : false,
                lazyFollow : true,
                lazyEffect : "fade",
                //Auto height
                autoHeight : false,
                //Mouse Events
                dragBeforeAnimFinish : true,
                mouseDrag : true,
                touchDrag : true,
                //Transitions
                transitionStyle : false,
                // Other
                addClassActive : false
            });
        },
        getImagesForCarousel = function (idForRestaurant) {
            $.getJSON('restaurant/' + idForRestaurant + '/pictures', function (response) {
                renderImagesForRestaurant(response);
            });
        },
        getImagesForRestaurants = function (listWithRestaurants) {
            for (var i = 0; i < listWithRestaurants.length; i++) {
                var idForRestaurant = listWithRestaurants[i].id;
                getImageForRestaurant(idForRestaurant, '#restaurant' + idForRestaurant);
            }
        },
        getImagesForProfileRestaurants = function (listWithRestaurants) {
            for (var i = 0; i < listWithRestaurants.length; i++) {
                var idForRestaurant = listWithRestaurants[i].id;
                getImageForRestaurant(idForRestaurant, '#restaurant' + idForRestaurant + "profile");
            }
        },
        /**
         * Gets the first image for restaurant with given id and appends the image to parent
         * container.
         * @param idForRestaurant
         * @param parent - identificator for parent container
         */
        getImageForRestaurant = function (idForRestaurant, parent) {
            $.getJSON('restaurant/' + idForRestaurant + '/pictures', function (response) {
                if (response.length > 0) {
                    //render image only the first one if it exists
                    var img = $('<img />');
                    img.attr('src', response[0].path);
                    img.addClass('restaurant-image');
                    $(parent).prepend(img);
                }
            });
        },
        showDivs = function (img) {
            $('img').removeClass("border-red");
            window.APP.selectedImg = img;
            img.toggleClass("border-red");
            $('#img-holder')[0].src = img[0].src;
            $('#img-path').val(img[0].src);
        },
        isValidImage = function (url) {
            $.ajax({
                       method: "HEAD",
                       url: "urlValue"
                   }).done(function(message,text,response){
                if(response.getResponseHeader('Content-Type').indexOf("image")===-1){
                    alert("not image");
                }
            });
        },
        clearMarkers = function () {
            for (var i = 0; i < markersArray.length; i++ ) {
                markersArray[i].setMap(null);
            }
        },
        placeMarker = function (location) {
            clearMarkers();
            var marker = new google.maps.Marker({
                position: location,
                map: window.APP.map2
            });
            markersArray.push(marker);
        },
        initMap = function (restaurantCoords, restaurantStreet) {
            var mapDiv = document.getElementById('map');
            var mapPopup = document.getElementById('map2');
            var streetInput = $("input[name='street']");
            var latInput = $("input[name='latitude']");
            var lngInput = $("input[name='longitude']");
            console.log(restaurantCoords);
            window.APP.map = new google.maps.Map(mapDiv, {
                center: restaurantCoords,
                zoom: 15,
                disableDefaultUI: true,
                disableDoubleClickZoom: true,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
                draggable: false,
                clickableIcons : false
            });
            window.APP.mainMarker = new google.maps.Marker({
                position: restaurantCoords,
                map: window.APP.map
            });
            window.APP.map2 = new google.maps.Map(mapPopup, {
                center: restaurantCoords,
                zoom: 15,
                draggableCursor:'crosshair',
                clickableIcons : false
            });
            latInput.val(restaurantCoords.lat);
            lngInput.val(restaurantCoords.lng);
            streetInput.val(restaurantStreet);
            var geocoder = new google.maps.Geocoder;


            placeMarker(restaurantCoords);
            google.maps.event.addListener(window.APP.map2, "click", function (event) {
                var latitude = event.latLng.lat();
                var longitude = event.latLng.lng();
                var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
                geocoder.geocode({'location': latlng}, function(results, status) {
                    if (status === 'OK') {
                        if (results[1]) {
                            streetInput.val(results[0].formatted_address);
                        } else {
                            streetInput.val("Unknown Location!");
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
                placeMarker({lat : latitude, lng : longitude});
                window.APP.mainMarker.setPosition( new google.maps.LatLng( latitude, longitude ) );
                window.APP.map.panTo( new google.maps.LatLng( latitude, longitude ) );
                latInput.val(latitude);
                lngInput.val(longitude);
            });
            google.maps.event.addListener(window.APP.map, "click", function (event) {
                $('#mapPopup').fadeIn(350);
                google.maps.event.trigger(window.APP.map2, "resize");
                window.APP.map2.panTo(restaurantCoords);
            });
        },
        initializeStaticGoogleMapInContainer = function (coordinates, container) {
            var mapProp = {
                center: coordinates,
                zoom: 15,
                disableDefaultUI: true,
                disableDoubleClickZoom: true,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
                draggable: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            return new google.maps.Map(document.getElementById(container), mapProp);
        },
        initializeDynamicGoogleMapInContainer = function (coordinates, container) {
            var mapProp = {
                center: coordinates,
                disableDefaultUI: false,
                zoom: 15,
                scrollwheel: true,
                navigationControl: true,
                mapTypeControl: true,
                scaleControl: true,
                draggable: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            return new google.maps.Map(document.getElementById(container), mapProp);
        },
        createPinWithColor = function(color) {
            return {
                path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
                fillColor: color,
                fillOpacity: 1,
                strokeColor: color,
                strokeWeight: 2,
                scale: 1,
                draggable: false
            };
        },
        createFixedMarkerForMap = function(map, coordinates, pin){
            var marker = new google.maps.Marker({
                position: coordinates,
                icon: pin
            });
            marker.setMap(map);
        },
        fillRestaurantData = function () {
            $('#img-wrapper').empty();
            $('#schedule-form').empty();
            $('#feature-form').empty();
            //-images
            var tpl = Handlebars.templates['imageUpdate'];
            $.getJSON('restaurant/' + $currentRestaurantId + '/pictures', function (response) {
                var count = 0;
                $.each(response, function (i, element) {
                    element.ind = i+1;
                    count++;
                });
                tpl = Handlebars.templates['imageUpdate'];
                $('#img-wrapper').html(tpl(response));
                $('.select')[0].click();
            });
                //-schedules
            $.getJSON('restaurant/' + $currentRestaurantId + '/schedule', function (data) {
                tpl = Handlebars.templates['timePicker'];
                $('#schedule-form').append(tpl(data));
                $('#dtBoxAdmin').DateTimePicker();
            });

            //-filters
            $.getJSON('filters', function (response) {
                console.log(response);
                window.APP = $.extend(window.APP, {filters: response})
            }).done(function (response) {
                $('#feature-form').html(Handlebars.templates['filtersUpdate'](window.APP.filters));
                $.getJSON('restaurant/' + $currentRestaurantId, function (response) {
                    $('#description').val(response.description);
                    $('#linkSocial').val(response.linkSocial);
                    $('#name').val(response.name);
                    $.each(response.features, function(i, item) {
                        $("input[name=" + item.name+ "]").prop( "checked", true );
                    });
                });
            });
            //-address
            $.getJSON('restaurant/' + $currentRestaurantId +'/address', function (response) {
                var coords ={
                    lat : response.latitude,
                    lng : response.longitude
                };
                initMap(coords, response.street);
            });
        },
        getTickets = function () {
            $('#reservations').empty();
            $.getJSON('restaurant/' + $currentRestaurantId +'/reservations', function (data) {
                var tpl = Handlebars.templates['reservationTicket'];
                $('#reservations').html(tpl(sortTicket(data)));
            });
        },
        sortTicket = function(tickets) {
            var sortedTickets = [];
            for (var i = 0; i < tickets.length; i++) {
                if (tickets[i].reservationStatus.localeCompare("CONFIRMED") === 0)
                    sortedTickets.push(tickets[i]);
            }
            for (var i = 0; i < tickets.length; i++) {
                if (tickets[i].reservationStatus.localeCompare("PENDING") === 0)
                    sortedTickets.push(tickets[i]);
            }
            return sortedTickets;
        },
        renderAdminPage = function (id) {
            $currentRestaurantId = id;
            $('body').removeClass('body-color');
            $('#mainContainer').empty();
            var tpl = Handlebars.templates['adminPage'];
            $mainContainer.html(tpl);
            fillRestaurantData(id);
            $('.bt-menu-trigger').remove();
            $('.search-container').remove();
            var num = Math.floor( Math.random() * 9 );
            $("#headerLogo").attr("src", "images/img"+num+".jpg");
            if (!$('#loginSection').hasClass('hidden')) {
                $('#loginSection').toggleClass('hidden');
            }
            getTickets();
            window.scrollTo(0, 0);
        };

    Handlebars.registerHelper('times', function (n, block) {
        var accumulator = '';
        for (var i = 2; i <= n; ++i)
            accumulator += block.fn(i);
        return accumulator;
    });

// This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            testAPI();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            document.getElementById('status').innerHTML = 'Please log ' +
                                                          'into this app.';
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            document.getElementById('status').innerHTML = 'Please log ' +
                                                          'into Facebook.';
        }
    }

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
                    appId      : '1767558840150355',
                    cookie     : true,  // enable cookies to allow the server to access
                                        // the session
                    xfbml      : true,  // parse social plugins on this page
                    version    : 'v2.5' // use graph api version 2.5
                });

        // Now that we've initialized the JavaScript SDK, we call
        // FB.getLoginStatus().  This function gets the state of the
        // person visiting this page and can return one of three states to
        // the callback you provide.  They can be:
        //
        // 1. Logged into your app ('connected')
        // 2. Logged into Facebook, but not your app ('not_authorized')
        // 3. Not logged into Facebook and can't tell if they are logged into
        //    your app or not.
        //
        // These three cases are handled in the callback function.

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });

    };

// Load the SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
        var id;
        FB.api('/me', function(response) {
            console.log('Successful login for: ' + response.id);
            document.getElementById('username').innerHTML =
                'You are logged in as  ' + response.name + '!';
            id = response.id;
        });
        FB.api('/me',{fields: 'id,name,email'}, function (response) {
            console.log(response);

        });
        FB.api('/me/picture', function (response) {
            console.log(response.data.url);
            document.getElementById('photo').innerHTML ="<img src='" + response.data.url + "'>";
            // $("#profilePicture").attr("src", response.data.url.toString() );

        })
    }







    Handlebars.registerHelper('if_eq', function(a, b, opts) {
        if (a === b) {
            return opts.fn(this);
        } else {
            return opts.inverse(this);
        }
    });

    Handlebars.registerHelper('formatTime', function(timeDTO) {
        //hour and min
        function checkTime(i) {
            return (i < 10) ? "0" + i : i;
        }
        return checkTime(timeDTO[0]) + ":" + checkTime(timeDTO[1]);
    });





// This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);

        if (response.status === 'connected') {
            // $('#faceLogButton').toggleClass('hidden');
            if ($('#faceLogButton').hasClass('hidden')){
                //console.log("mere treaba");
            }else{
                $('#faceLogButton').toggleClass('hidden');
            }
            $('#profileBtn').removeClass('hidden');
            $('#btnLogOut').removeClass('hidden');

            // Logged into your app and Facebook.
            getFaceData();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            cleanCashClient();
            $('#faceLogButton').removeClass('hidden');
            // $('#profileBtn').toggleClass('hidden');
            // $('#btnLogOut').toggleClass('hidden');
            document.getElementById('usernameFace').innerHTML = '';
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            $('#faceLogButton').removeClass('hidden');
            if (! $('#btnLogOut').hasClass('hidden')) {
                $('#btnLogOut').toggleClass('hidden');
                $('#profileBtn').toggleClass('hidden');
            }
            document.getElementById('usernameFace').innerHTML = '';
            cleanCashClient();

        }
    }

    function cleanCashClient() {
        window.APP.clientName = '';
        window.APP.clientId = '';
        window.APP.clientEmail = '';
        window.APP.clientPicture = '';
        $('#faceLogButton').removeClass('hidden');
        if (! $('#btnLogOut').hasClass('hidden')) {
            $('#profileBtn').toggleClass('hidden');
            $('#btnLogOut').toggleClass('hidden');
        }
        document.getElementById("photo").innerHTML = '';

    }
    function logout(){
        var response = {};
        response.status = 'not_authorized';

        statusChangeCallback(response);


        FB.api('/me/permissions', 'delete', function(response) {
            console.log(response); // true
        });


    }

    function clientSave () {
        var name = window.APP.clientName;
        var email = window.APP.clientEmail;
        var json = { 'id' : 0,'name': name,'email' : email};
        $.ajax({
                   contentType:'application/json',
                   method:'POST',
                   url:'client',
                   data:JSON.stringify(json),
                   success: function(response) {
                       window.APP.clientDbId = response.id;
                       console.log(response);
                   }

               });
    }

    window.checkLoginState = function () {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    };

    function prepareFacebook(){
        window.fbAsyncInit = function() {
            FB.init({
                        appId      : '1767558840150355',
                        cookie     : true,  // enable cookies to allow the server to access
                                            // the session
                        xfbml      : true,  // parse social plugins on this page
                        version    : 'v2.5' // use graph api version 2.5
                    });
            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });
        };
    }

// Load the SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function getFaceData() {
        console.log('Welcome!  Fetching your information.... ');
        var id;
        FB.api('/me', function(response) {
            console.log('Successful login for: ' + response.id);
            document.getElementById('usernameFace').innerHTML =
                'Hello, ' + response.name + '!';
            id = response.id;
        });
        FB.api('/me',{fields: 'id,name,email'}, function (response) {
            window.APP.clientName = response.name;
            window.APP.clientId = response.id;
            window.APP.clientEmail = response.email;clientSave();

        });
        FB.api('/me/picture', function (response) {
            window.APP.clientPicture = response.data.url;
            console.log(response.data.url);
            document.getElementById('photo').innerHTML ="<img  src='" + response.data.url + "'class='user-photo'>";
            // $("#profilePicture").attr("src", response.data.url.toString() );

        })
    }


    /*** [START] Defining routes ***/
    Path.map("#index").to(function () {
        renderHomePage();
    });
    Path.map("#restaurant/:restaurantId/reservation/:reservationId").to(function () {
        var num = Math.floor( Math.random() * 9 );
        $("#headerLogo").attr("src", "images/img"+num+".jpg");
        renderReview(this.params['restaurantId'],this.params['reservationId']);
    });
    Path.map("#search/:query").to(function () {
        $.getJSON('restaurant/search', {query: this.params['query']}, function (response) {
            renderRestaurants(response);
        });
    });

    Path.map("#authenticate").to(function () {
        renderLogin();
    });

    Path.map("#restaurant/:id").to(function () {
        var idForRestaurant = this.params['id'];
        //id contains the word restaurant and some number which is the actual id for restaurant
        idForRestaurant = idForRestaurant.replace('restaurant','');
        $.ajax({
           contentType:'application/json',
           method:'GET',
           url:'restaurant/' + idForRestaurant
        }).done(function (response) {
            $currentRestaurantId=idForRestaurant;
            renderRestaurantDetailsPage(response);
        });
    });

    Path.map("#restaurant/:RId/admin").to(function () {
        console.log('restaurant: ', this.params['RId']);
        renderAdminPage(this.params['RId']);
    });

    Path.rescue(function () {
        window.location.href = '404.html';
    });

    Path.root("#index");
    /*** [END] Defining routes ***/


    $(document).ready(function () {
        Path.listen();
        bindEventHandlers();
    });
}());