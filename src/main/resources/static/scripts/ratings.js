(function ($, NS) {
    window[NS] = $.extend(window[NS] || {}, {
        floatToOneDigit: function (data) {
            data['foodRating'] = data['foodRating'].toFixed(1);
            data['ambienceRating'] = data['ambienceRating'].toFixed(1);
            data['valueRating'] = data['valueRating'].toFixed(1);
            data['serviceRating'] = data['serviceRating'].toFixed(1);
            return data;
        },
        renderRatings: function (data, targetContainer) {
            if (!targetContainer instanceof jQuery) {
                targetContainer = $(targetContainer)
            }
            var tpl = Handlebars.templates['ratingreview'];
            data = this.floatToOneDigit(data);
            data['rating'] = data['rating'].toFixed(1);
            targetContainer.html(tpl(data));
            $('#starPercentage').width(Math.floor(data['rating']) * 16 + (data['rating'] % 1) * 16 + 2);
        },
        renderComments: function (data, targetContainer) {
            if (!targetContainer instanceof jQuery) {
                targetContainer = $(targetContainer)
            }
            for (var i = 0; i < data.length; ++i) data[i] = {review: this.floatToOneDigit(data[i])};
            var overAllRating = 0;
            for (i = 0; i < data.length; ++i) {
                overAllRating += parseInt(data[i]['review']['foodRating']);
                overAllRating += parseInt(data[i]['review']['valueRating']);
                overAllRating += parseInt(data[i]['review']['ambienceRating']);
                overAllRating += parseInt(data[i]['review']['serviceRating']);
                data[i]['review']['rating'] = (overAllRating / 4).toFixed(1);
                var monthNames = {
                    1: "January", 2: "February", 3: "March", 4: "April", 5: "May", 6: "June",
                    7: "July", 8: "August", 9: "September", 10: "October", 11: "November", 12: "December"
                };
                var year = data[i]['review']['date'][0];
                var month = data[i]['review']['date'][1];
                var day = data[i]['review']['date'][2];
                if (parseInt(day) < 10) day = '0' + day;
                data[i]['review']['date'] = day + '/' + monthNames[month] + '/' + year;
                overAllRating = 0;
            }
            var tpl = Handlebars.templates['reviewComments'];
            targetContainer.html(tpl({reviews: data}));
            for (i = 0; i < data.length; ++i) {
                $('#stars' + data[i]['review']['id']).width(Math.floor(data[i]['review']['rating']) * 16 + (data[i]['review']['rating'] % 1) * 16+2)
            }
        }
    });
}(jQuery, 'RTG'));
